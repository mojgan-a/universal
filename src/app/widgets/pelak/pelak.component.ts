import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-pelak',
    templateUrl: './pelak.component.html',
    styleUrls: ['./pelak.component.scss']
})
export class PelakComponent implements OnInit {
    @Input() iranCode: string;
    @Input() code1: string;
    @Input() code2: string;
    @Input() code3: string;
    constructor() {}

    ngOnInit() {}
}
