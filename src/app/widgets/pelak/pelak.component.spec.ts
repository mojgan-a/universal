import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PelakComponent } from './pelak.component';

describe('PelakComponent', () => {
    let component: PelakComponent;
    let fixture: ComponentFixture<PelakComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [PelakComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PelakComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
