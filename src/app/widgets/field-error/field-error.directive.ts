import {
    Directive,
    DoCheck,
    ElementRef,
    Input,
    Renderer2
} from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { ErrorHandlingService } from '../../core/error/error-handling.service';

@Directive({
    selector: '[appHasError]'
})
export class FieldErrorDirective implements DoCheck {
    private _control: AbstractControl;

    constructor(private _el: ElementRef, private _renderer: Renderer2) {}

    @Input()
    set appHasError(control) {
        this._control = control;
    }

    ngDoCheck() {
        this.checkControl();
    }

    public checkControl() {
        if (this._showErrorSection(this._control)) {
            this._addClass('error');
        } else {
            this._removeClass('error');
        }
    }

    private _addClass(className) {
        if (!this._el.nativeElement.classList.contains(className)) {
            this._renderer.addClass(this._el.nativeElement, className);
        }
    }

    private _removeClass(className) {
        if (this._el.nativeElement.classList.contains(className)) {
            this._renderer.removeClass(this._el.nativeElement, className);
        }
    }

    private _showErrorSection(control) {
        return control.invalid && (control.dirty || control.touched);
    }
}

@Directive({
    selector: '[appTooltipError]'
})
export class FieldErrorTooltipDirective implements DoCheck {
    private _control: AbstractControl;

    constructor(
        private _el: ElementRef,
        private _renderer: Renderer2,
        private _eh: ErrorHandlingService
    ) {}

    @Input()
    set appTooltipError(control) {
        this._control = control;
    }

    ngDoCheck() {
        this.checkControl();
    }

    public checkControl() {
        if (this._showErrorSection(this._control)) {
            this._addTooltip();
        } else {
            this._removeTooltip();
        }
    }

    private _addTooltip() {
        if (!this._el.nativeElement.classList.contains('tooltip-danger')) {
            this._renderer.addClass(this._el.nativeElement, 'tooltip-danger');
        }
        this._renderer.setAttribute(
            this._el.nativeElement,
            'data-tooltip',
            this._getLastError()
        );
    }

    private _removeTooltip() {
        if (this._el.nativeElement.classList.contains('tooltip-danger')) {
            this._renderer.removeClass(
                this._el.nativeElement,
                'tooltip-danger'
            );
        }
        this._renderer.removeAttribute(this._el.nativeElement, 'data-tooltip');
    }

    private _getLastError(): string {
        const formErrors = this._control.errors;
        const errors = [];
        Object.keys(formErrors).forEach(key => {
            if (formErrors[key]) {
                errors.push(this._eh.getErrorMessage(key));
            }
        });
        return errors[0];
    }

    private _showErrorSection(control) {
        return control.invalid && (control.dirty || control.touched);
    }
}
