import { Component, OnInit, Input } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { ErrorHandlingService } from '../../core/error/error-handling.service';

@Component({
    selector: 'app-field-error',
    templateUrl: './field-error.component.html',
    styleUrls: ['./field-error.component.scss']
})
export class FieldErrorComponent implements OnInit {
    @Input() inputControl: AbstractControl;
    constructor(private _eh: ErrorHandlingService) {}

    ngOnInit() {}

    shouldShowErrorSection() {
        return (
            this.inputControl.invalid &&
            (this.inputControl.dirty || this.inputControl.touched)
        );
    }

    getRegisterFormErrors() {
        const formErrors = this.inputControl.errors;
        const errors = [];
        Object.keys(formErrors).forEach(key => {
            if (formErrors[key]) {
                errors.push(this._eh.getErrorMessage(key));
            }
        });
        return errors;
    }
}
