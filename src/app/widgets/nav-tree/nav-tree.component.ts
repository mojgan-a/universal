import { Component, Input, OnInit } from '@angular/core';
import { Nav } from '../../core/navigation/navigation.service';

@Component({
    selector: 'app-nav-tree',
    templateUrl: './nav-tree.component.html',
    styleUrls: ['./nav-tree.component.scss']
})
export class NavTreeComponent implements OnInit {
    @Input() nav: Nav;
    constructor() {}

    ngOnInit() {}
}
