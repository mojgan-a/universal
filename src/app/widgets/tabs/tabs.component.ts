import {
    Component,
    ContentChildren,
    ElementRef,
    Input,
    OnInit,
    QueryList,
    ViewChild
} from '@angular/core';
import { Subject } from 'rxjs';
import { TabComponent } from './tab/tab.component';

@Component({
    selector: 'tabs',
    templateUrl: './tabs.component.html',
    styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit {
    @Input() activeTab: Subject<number>;
    @ViewChild('tabDiv') tabDiv;

    @ContentChildren(TabComponent) tabs: QueryList<TabComponent>;

    constructor(private elt: ElementRef) {}

    ngOnInit() {
        if (this.activeTab) {
            this.activeTab.subscribe(tabIndex => {
                this._changeActiveTab(tabIndex);
            });
        }
    }

    ngAfterContentInit() {
        this.tabs.first.show();
    }

    changeTab(tab) {
        this.currentTab.hide();
        tab.show();
    }

    private _changeActiveTab(tabIndex) {
        let tabs = this.tabs.toArray();
        if (tabs[tabIndex]) {
            if (this.currentTab) {
                this.currentTab.hide();
            }
            tabs[tabIndex].show();
        }
    }

    get currentTab() {
        return this.tabs.find(r => {
            return r.isShow;
        });
    }

    get currentTabIndex() {
        return this.tabs.toArray().indexOf(this.currentTab);
    }

    get tabWidth() {
        return this.tabDiv.nativeElement.clientWidth / this.tabs.length - 1;
    }
}
