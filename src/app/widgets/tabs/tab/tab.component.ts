import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss']
})
export class TabComponent implements OnInit {
  @Input() title: string;
  content: string;
  _show: boolean = false;

  constructor() { }

  ngOnInit() {}

  show() {
    this._show = true;
  }

  hide() {
    this._show = false;
  }

  get isShow() {
    return this._show;
  }

}
