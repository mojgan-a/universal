import {
    Component,
    ElementRef,
    EventEmitter,
    Input,
    OnInit,
    Output,
    Renderer2,
    ViewChild
} from '@angular/core';
import * as _ from 'lodash/core';
import {
    humanizeBytes,
    UploaderOptions,
    UploadFile,
    UploadInput,
    UploadOutput,
    UploadStatus
} from 'ngx-uploader';
import { environment } from '../../../environments/environment';
import { AuthService } from '../../core/auth.service';

const BaseUrl = environment.baseUrl;

export interface PictureUploaderOption {
    uploadUrl: string;
    picturePath?: string;
    canDelete?: boolean;
    maxSize?: number;
    needAuth?: boolean;
    metaData?: { [key: string]: string };
}

export interface UploadEvent {
    isOk: boolean;
    response: any;
}

@Component({
    selector: 'app-picture-uploader',
    styleUrls: ['./pictureUploader.scss'],
    templateUrl: './pictureUploader.html'
})
export class PictureUploaderComponent implements OnInit {
    @Input() option: PictureUploaderOption;
    @Output() upload = new EventEmitter<UploadFile>();
    @Output() rejected = new EventEmitter<string>();
    @Output() uploadCompleted = new EventEmitter<UploadEvent>();
    @ViewChild('fileUpload') public _fileUpload: ElementRef;

    public uploadInput: EventEmitter<UploadInput> = new EventEmitter<
        UploadInput
    >();
    public humanizeBytes: Function = humanizeBytes;
    public uploadInProgress: Boolean = false;
    public picturePath: string;
    public picture: UploadFile = null;
    public allowedContentTypes = [
        'image/png',
        'image/jpeg',
        'image/jpg',
        'image/bmp'
    ];
    public uploaderOptions: UploaderOptions = {
        concurrency: 1,
        allowedContentTypes: this.allowedContentTypes
    };

    constructor(private renderer: Renderer2, private auth: AuthService) {}
    ngOnInit() {
        
        if (!this.option.picturePath) {
            this.option.picturePath = 'assets/images/user.jpg';
        }

        if (!this.option.metaData) {
            this.option.metaData = {};
        }

        if (_.isUndefined(this.option.canDelete)) {
            this.option.canDelete = true;
        }

        try {
            if (this.option.uploadUrl.slice(0, 2) === '..') {
                this.option.uploadUrl =
                    BaseUrl + this.option.uploadUrl.substring(3);
            }
        } catch (err) {
            throw new Error(
                'picture uploader need option that containe upload url'
            );
        }
        this.picturePath = this.option.picturePath;
    }

    onUploadOutput(output: UploadOutput): void {
        console.log(output.type, output);
        if (
            output.type === 'addedToQueue' &&
            typeof output.file !== 'undefined'
        ) {
            if (this.allowedContentTypes.indexOf(output.file.type) === -1) {
                this.uploadInput.emit({ type: 'cancel' });
                this.rejected.emit('fileTypeNotAllowed');
            }
            if (this.option.maxSize && output.file.size > this.option.maxSize) {
                this.uploadInput.emit({ type: 'cancel' });
                this.rejected.emit('fileSizeLarge');
            }
            this.picture = output.file;

            const event: UploadInput = {
                type: 'uploadAll',
                url: this.option.uploadUrl,
                method: 'POST',
                data: this.option.metaData
            };
            if (this.option.needAuth) {
                event.headers = {
                    Authorization: 'Bearer ' + this.auth.getToken()
                };
            }
            setTimeout(() => {
                this.uploadInput.emit(event);
            }, 0);
            this.upload.emit(output.file);
        } else if (
            output.type === 'uploading' &&
            typeof output.file !== 'undefined'
        ) {
            this.picture = output.file;
        } else if (output.type === 'removed') {
            this.picture = null;
        } else if (output.type === 'done') {
            this.picture = output.file;
            let isOk = false;
            const fileEvent = output.file;
            fileEvent.nativeFile = output.nativeFile;
            if (
                this.picture.responseStatus <= 299 &&
                this.picture.responseStatus >= 200
            ) {
                this._showPictureFromeFile();
                isOk = true;
            }
            this.uploadCompleted.emit({
                isOk: isOk,
                response: this.picture.response
            });
        }
    }

    bringFileSelector(): boolean {

        // this.renderer.selectRootElement('fileUpload').scrollIntoView();
        // this.renderer.invokeElementMethod(
        //     this._fileUpload.nativeElement,
        //     'click'
        // );
        this._fileUpload.nativeElement.click();
        return false;
    }

    closeClick() {
        if (!this.picture) {
            return;
        }
        if (this.picture.progress.status === UploadStatus.Uploading) {
            this._cancelUpload();
        }
        this._removePicture();
    }

    private _removePicture(): void {
        this.uploadInput.emit({ type: 'removeAll' });
        this.picture = null;
    }

    private _cancelUpload(): void {
        this.uploadInput.emit({ type: 'cancel', id: this.picture.id });
    }

    _showPictureFromeFile(): void {
        const files = this._fileUpload.nativeElement.files;
        const reader = new FileReader();

        reader.addEventListener(
            'load',
            (event: Event) => {
                this.picturePath = (<any>event.target).result;
            },
            false
        );

        if (files.length) {
            const file = files[0].slice(0, files[0].size, files[0].type);
            reader.readAsDataURL(file);
        }
    }
}
