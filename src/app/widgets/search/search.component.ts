
import {fromEvent as observableFromEvent,  Observable ,  Subscription } from 'rxjs';

import {debounceTime, map} from 'rxjs/operators';
import {
    AfterViewInit,
    Component,
    ElementRef,
    EventEmitter,
    Input,
    Output,
    ViewChild
} from '@angular/core';

export interface SearchOption {
    placeholder?: string;
}

@Component({
    selector: 'search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss']
})
export class SearchComponent implements AfterViewInit {
    @Input() value: any = null;
    @Input() datas: any[];
    @Input()
    options: SearchOption = {
        placeholder: ''
    };
    @Output() changeTerm = new EventEmitter();
    @Output() chageValue = new EventEmitter();

    @ViewChild('input') inputText: ElementRef;

    public dropOpen: boolean = false;
    private _valueChange: Observable<any>;
    private _valueChangeSubscription: Subscription;

    constructor() {}

    ngAfterViewInit() {
        this._valueChange = observableFromEvent(
            this.inputText.nativeElement,
            'keyup'
        ).pipe(map((x: any) => x.currentTarget.value));
        this._valueChangeSubscription = this._valueChange.pipe(
            debounceTime(1000))
            .subscribe(value => this.check(value));
    }

    onBlur() {
        setTimeout(() => {
            if (this.value && this.value.title) {
                this.inputText.nativeElement.value = this.value.title;
            } else {
                this.inputText.nativeElement.value = '';
            }
            this.dropOpen = false;
        }, 600);
    }

    onFocus() {
        this.dropOpen = true;
    }

    check(value) {
        console.log(value);
        this.changeTerm.emit(value);
    }

    selectOption(val) {
        this.value = val;
        this.chageValue.emit(val);
    }

    ngOnDestroy() {
        this._valueChangeSubscription.unsubscribe();
    }
}
