import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgUploaderModule } from 'ngx-uploader';
import { FieldErrorComponent } from './field-error/field-error.component';
import {
    FieldErrorDirective,
    FieldErrorTooltipDirective
} from './field-error/field-error.directive';
import { InputComponent } from './input/input.component';
import { ModalComponent } from './modal/modal.component';
import { NavTreeComponent } from './nav-tree/nav-tree.component';
import { PelakComponent } from './pelak/pelak.component';
import { PictureUploaderComponent } from './pictureUploader/pictureUploader.component';
import { RialPipe } from './pipes/rial.pipe';
import { SeparatorPipe } from './pipes/separator.pipe';
import { SearchComponent } from './search/search.component';
import { SpinerComponent } from './spiner/spiner.component';
import { TabComponent } from './tabs/tab/tab.component';
import { TabsComponent } from './tabs/tabs.component';

const widgets = [
    PelakComponent,
    PictureUploaderComponent,
    FieldErrorTooltipDirective,
    FieldErrorDirective,
    FieldErrorComponent,
    TabComponent,
    NavTreeComponent,
    InputComponent,
    SpinerComponent,
    TabsComponent,
    SearchComponent,
    ModalComponent,
    SeparatorPipe,
    RialPipe
];

@NgModule({
    imports: [
        CommonModule,
        NgUploaderModule,
        RouterModule,
        ReactiveFormsModule
    ],
    declarations: [...widgets],
    exports: [...widgets]
})
export class WidgetsModule {}
