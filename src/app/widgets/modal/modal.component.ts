import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
    @Input() show: boolean;
    
    constructor() {
        console.log("[ReserveComponent] constructor is called, this.show:", this.show);

    }

    ngOnInit() {
        
    }
    close() {
        this.show = false;
        console.log("[ModalComponent] close, this.show:", this.show);
    }

    
    
}
