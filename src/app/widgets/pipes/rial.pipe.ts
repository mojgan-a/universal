import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'rial'
})
export class RialPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return `<span style="direction: rtl;">${ value } ریال </span>`;
  }

}
