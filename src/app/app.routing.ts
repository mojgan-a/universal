import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { InternalServerErrorComponent } from './components/internal-server-error/internal-server-error.component';
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';
import { AuthGuard } from './core/auth.guard';
import { ConfirmationDialogResolver } from './components/confirmation-dialog/confirmation-dialog.service';

export const routes: Routes = [
    { path: '404', component: NotFoundComponent },
    { path: '500', component: InternalServerErrorComponent },
    {
        path: 'vehicle',
        loadChildren: 'app/exprs/vehicle/vehicle.module#VehicleModule',
        canActivate: [AuthGuard]
    },
    {
        path: 'service-center',
        loadChildren:
            'app/exprs/service-center/service-center.module#ServiceCenterModule'
    },
    { path: 'user', loadChildren: 'app/exprs/user/user.module#UserModule' },
    {
        path: 'request',
        loadChildren: 'app/exprs/request/request.module#RequestModule',
        // canActivate: [AuthGuard]
    },
    {
        path: 'transactions',
        loadChildren:
            'app/exprs/transactions/transactions.module#TransactionsModule',
        canActivate: [AuthGuard]
    },
    {
        path: 'confirm',
        component: ConfirmationDialogComponent,
        outlet: 'popup',
        resolve: {
            message: ConfirmationDialogResolver
        }
    },
    { path: '', redirectTo: 'service-center', pathMatch: 'full' },
    { path: '**', redirectTo: '404' }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes, {
    useHash: true,
    enableTracing: false
});
