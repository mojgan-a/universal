import { Component, OnInit } from '@angular/core';
import {
    Nav,
    NavigationService
} from '../../../core/navigation/navigation.service';

@Component({
    selector: 'app-side-bar',
    templateUrl: './side-bar.component.html',
    styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit {
    nav: Nav;
    constructor(private _navService: NavigationService) {}

    ngOnInit() {
        this.nav = this._navService.getNav('side-bar');
    }
}
