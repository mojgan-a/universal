import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from '../../../core/auth.service';
import { LayoutService } from '../../../core/layout.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    @Output() toggleRightSidebar = new EventEmitter<boolean>();
    @Output() toggleLeftSidebar = new EventEmitter<boolean>();
    @Input() isOpenRight: boolean;
    @Input() isOpenLeft: boolean;
    @Input() canToggleRight = false;
    public isLogin = false;
    public avatarImagePath = 'assets/images/avatar.png';
    private _getUserImageSubscription: Subscription;
    private _getUserNotificationSubscription: Subscription;
    constructor(private _ls: LayoutService, private _auth: AuthService) {}

    ngOnInit() {
        this.isLogin = this._ls.isLoginNow();
        if (this.isLogin) {
            this._getUserImageSubscribe();
            // this._getUserNotificationSubscribe();
        }

        this._ls.isLogin().subscribe(flag => {
            this.isLogin = flag;

            if (
                flag &&
                !this._getUserImageSubscription &&
                !this._getUserNotificationSubscription
            ) {
                this._getUserImageSubscribe();
                // this._getUserNotificationSubscribe();
            }

            if (
                !flag &&
                this._getUserImageSubscription &&
                this._getUserNotificationSubscription
            ) {
                this._getUserImageSubscription.unsubscribe();
                // this._getUserNotificationSubscription.unsubscribe();
            }
        });
    }

    private _getUserImageSubscribe(): void {
        this._getUserImageSubscription = this._ls
            .getUserImagePath()
            .subscribe(path => {
                this.avatarImagePath = path;
            });
    }

    private _getUserNotificationSubscribe(): void {
        this._getUserNotificationSubscription = this._ls
            .getNotifications()
            .subscribe(res => {
                console.log(res);
            });
    }

    logout() {
        this._ls.logout();
    }

    onToggleRightSidebar() {
        this.toggleRightSidebar.emit();
    }

    onToggleLeftSidebar() {
        this.toggleLeftSidebar.emit();
    }
}
