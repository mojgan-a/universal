import { Component, HostBinding, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationDialogService } from './confirmation-dialog.service';

@Component({
    selector: 'app-confirmation-dialog',
    templateUrl: './confirmation-dialog.component.html',
    styleUrls: ['./confirmation-dialog.component.scss']
})
export class ConfirmationDialogComponent implements OnInit {
    @HostBinding('style.display') display = 'block';
    @HostBinding('style.position') position = 'fixed';
    @HostBinding('style.z-index') zindex = 3000;
    public title: string;

    constructor(
        private cs: ConfirmationDialogService,
        private route: ActivatedRoute,
        private router: Router
    ) {}

    ngOnInit() {
        this.route.data.subscribe((data: { message: string }) => {
            this.title = data.message;
        });
    }

    cancel() {
        this.cs.cancel();
    }

    confirm() {
        this.cs.accept();
    }
}
