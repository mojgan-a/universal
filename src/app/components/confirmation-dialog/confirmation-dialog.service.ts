
import {of as observableOf,  Observable } from 'rxjs';
import { EventEmitter, Injectable } from '@angular/core';
import {
    ActivatedRouteSnapshot,
    Resolve,
    Router,
    RouterStateSnapshot
} from '@angular/router';

@Injectable()
export class ConfirmationDialogService {
    public confirmEvent: EventEmitter<any> = new EventEmitter();
    private _message: string;

    constructor(private router: Router) {
        this.closePopup();
    }

    confirm(message: string): Observable<boolean> {
        this._message = message;
        this.router.navigate([{ outlets: { popup: 'confirm' } }]);
        return Observable.create(observer => {
            this.confirmEvent.subscribe(confirmed => {
                observer.next(confirmed);
                this._message = '';
                this.closePopup();
                observer.complete();
            });
        });
    }

    cancel() {
        this.confirmEvent.emit(false);
    }

    accept() {
        this.confirmEvent.emit(true);
    }

    closePopup() {
        this.router.navigate([{ outlets: { popup: null } }]);
    }

    getMessage() {
        return this._message;
    }
}

@Injectable()
export class ConfirmationDialogResolver implements Resolve<string> {
    constructor(
        private cs: ConfirmationDialogService,
        private router: Router
    ) {}

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<string> {
        return observableOf(this.cs.getMessage());
    }
}
