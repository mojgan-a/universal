import { BrowserModule } from '@angular/platform-browser';
import { BrowserTransferStateModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { ToastModule } from 'ng6-toastr/ng2-toastr';
import { CoreModule } from './core/core.module';
import { routing } from './app.routing';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/layout/header/header.component';
import { SideBarComponent } from './components/layout/side-bar/side-bar.component';
import { FooterComponent } from './components/layout/footer/footer.component';
import { NotificationComponent } from './components/layout/notification/notification.component';
import { WidgetsModule } from './widgets/widgets.module';
import { InternalServerErrorComponent } from './components/internal-server-error/internal-server-error.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';
import {
    ConfirmationDialogService,
    ConfirmationDialogResolver
} from './components/confirmation-dialog/confirmation-dialog.service';
// import { Ng2DeviceDetectorModule } from 'ng2-device-detector';

@NgModule({
    imports: [
        BrowserAnimationsModule,
        CoreModule,
        LeafletModule.forRoot(),
        ToastModule.forRoot(),
        // Ng2DeviceDetectorModule.forRoot(),
        WidgetsModule,
        routing,
        BrowserModule.withServerTransition({ appId: 'rf' }), // <-- here
        BrowserTransferStateModule
    ],
    declarations: [
        AppComponent,
        HeaderComponent,
        SideBarComponent,
        FooterComponent,
        NotificationComponent,
        InternalServerErrorComponent,
        NotFoundComponent,
        ConfirmationDialogComponent
    ],
    providers: [ConfirmationDialogService, ConfirmationDialogResolver],
    bootstrap: [AppComponent]
})
export class AppModule {}
