import { Component, OnInit } from '@angular/core';
import { ActivationStart, Router,NavigationEnd } from '@angular/router';
import * as _ from 'lodash';

import { AuthService } from './core/auth.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    public title = 'app';
    public rightSideOpen = true;
    public leftSideOpen = false;
    public widthScreen = true;
    public layout = {
        footer: true,
        header: true,
        aside: true
    };
    private _layoutDefults = {
        footer: true,
        header: true,
        aside: true
    };
    private _widthMenuBraeckPoint: number;
    static convertRemToPixels(rem) {
        return (
            rem *
            parseFloat(getComputedStyle(document.documentElement).fontSize)
        );
    }

    // constructor(private _router: Router, private _auth: AuthService) {
    //     this._widthMenuBraeckPoint = AppComponent.convertRemToPixels(46);
    //     _router.events
    //         .filter(event => {
    //             return event instanceof ActivationStart;
    //         })
    //         .subscribe((event: ActivationStart) => {
    //             this.layout = _.clone(this._layoutDefults);
    //             if (event.snapshot.data.layout) {
    //                 this.checkChange(event.snapshot.data.layout);
    //             }
    //         });
    //     window.addEventListener('resize', this.onResize.bind(this));
    // }

    constructor(private router: Router,private _auth: AuthService) {
        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                console.log('appcomponentEvent',event,event instanceof NavigationEnd);
                (<any>window).ga('set', 'page', event.urlAfterRedirects);
                (<any>window).ga('send', 'pageview');
         }
       });
     }

    ngOnInit() {
        this._auth.authStatus.subscribe(status => {
            if (!status) {
                this.leftSideOpen = false;
            }
        });
    }

    private onResize() {
        console.log(this._widthMenuBraeckPoint);
        if (window.screen.width < this._widthMenuBraeckPoint) {
            this.rightSideOpen = false;
        } else {
            this.rightSideOpen = true;
        }
    }

    checkChange(data) {
        this.layout.header = _.isNil(data.header)
            ? this._layoutDefults.header
            : data.header;
        this.layout.aside = _.isNil(data.aside)
            ? this._layoutDefults.aside
            : data.aside;
        this.layout.footer = _.isNil(data.footer)
            ? this._layoutDefults.footer
            : data.footer;
    }

    toggleRightSidebar() {
        this.rightSideOpen = !this.rightSideOpen;
        console.log(this.rightSideOpen);
    }

    toggleLeftSidebar() {
        this.leftSideOpen = !this.leftSideOpen;
    }
}
