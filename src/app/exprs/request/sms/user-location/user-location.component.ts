
import { Component, OnInit, ViewContainerRef } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as moment from "jalali-moment";
import { Service } from "../../../service-center/service-center.model";
import { CarModel, LPN } from "../../../vehicle/vehicle.model";
import { UserLocationService } from "./user-location.service";
import { userRequest } from "./userLocationModel";
import { ToastsManager } from 'ng6-toastr/ng2-toastr';
// import { AddVehicleService } from '../../../vehicle/add-vehicle/add-vehicle.service';

interface _Service extends Service {
    checked: boolean;
}

@Component({
    selector: 'app-user-location',
    templateUrl: './user-location.component.html',
    styleUrls: ['./user-location.component.scss']
})
export class UserLocationComponent implements OnInit {
    public moment = moment;
    public form = {
        car: null,
        dateTime: 0,
        description: "",
        services: []
    };

    public services: _Service[];
    public providerId: string;
    public totlePrice = "2000";
    public selectedDate;
    public selectedLpn: LPN;

    reserveModel = new userRequest("", "", "", "", "", "", "", "", "", "", "", "","","");

    // reserveLpnModel = new reserveLpn(
    //     "","",""
    // );
    submitted = false;
    onSubmit() {
        this.submitted = true;
        console.log(this.reserveModel);
        this.reserveModel.providerId = this.providerId;
        this.api.addReserve(this.reserveModel).subscribe(res => {
            if (res.ok) {
                this._tost.success(' درخواست شما با موفقیت ثبت شد. کد پیگیری شما: '+res.message);
            } else {
                this._tost.error('خطا در سرور!');
            }
        },
        err => {
            console.error(err);
            
            this._tost.error('لطفا تمامی فیلدها را کامل کنید.');
        });
    }
    constructor(
        private api: UserLocationService,
        private _fb: FormBuilder,
        private _sendService: UserLocationService,
        private _router: Router,
        private _route: ActivatedRoute,
         // private _addVehicleService: AddVehicleService
        private _tost: ToastsManager ,
        private _vcr: ViewContainerRef

    ) {
        this._tost.setRootViewContainerRef(_vcr);

    }
    ngOnInit() {
        this.providerId = this._route.snapshot.params["id"];
        this.selectedLpn = {
            code1: "",
            code2: "",
            code3: "",
            iranCode: ""
        };
        console.log(this.selectedLpn);
    }
}
