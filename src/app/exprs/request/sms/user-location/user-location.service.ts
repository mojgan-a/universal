
import {map} from 'rxjs/operators';

import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { BaseService } from "../../../../core/base-service";
import {
    CarModel,
    ColorModel,
    CompanyModel,
    LPN,
    LPNModel,
    ModelModel,    ValidationResponse

} from "../../../vehicle/vehicle.model";
import { userRequest } from "./userLocationModel";

@Injectable()
export class UserLocationService extends BaseService {
    constructor(private http: HttpClient) {
        super();
    }

    addReserve(userRequest: userRequest) {
        console.log("1:", userRequest.code1);
        console.log("2:", userRequest.code2);
        console.log("3:", userRequest.code3);
        console.log("ic:", userRequest.iranCode);
        return this.http.post(this._apiUrl.reserve, {
            firstname: userRequest.firstname,
            lastname: userRequest.lastname,
            model: userRequest.model,
            lpn: userRequest.code1+userRequest.code2+userRequest.code3+userRequest.iranCode,
            code1 : userRequest.code1,
            code2 : userRequest.code2,
            code3 :userRequest.code3,
            iranCode: userRequest.iranCode,
            date: userRequest.date,
            time: userRequest.time,
            mobile: userRequest.mobile,
            service: userRequest.service,
            voucher: userRequest.voucher,
            description:userRequest.description,
            //   latitude
            //   longitude     :userRequest.
            providerId: userRequest.providerId
        }).pipe(map((res: any) => {
            return new ValidationResponse(res.message);
        }));


    }
}
