export class userRequest {
    constructor(
        public firstname: string,
        public lastname: string,
        public model: string,
        public code1: string,
        public code2: string,
        public code3: string,
        public iranCode: string,
        public date: string,
        public time: string,
        public mobile: string,
        public service: string,
        public voucher: string,
        public description: string,
        public providerId: string
    ) {}
}

