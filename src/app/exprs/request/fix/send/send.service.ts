
import {map} from 'rxjs/operators';
import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { BaseService } from "../../../../core/base-service";
import {
  CarModel,
  ColorModel,
  CompanyModel,
  LPN,
  LPNModel,
  ModelModel,
  ValidationResponse
} from "../../../vehicle/vehicle.model";
import { reserve } from "./reserveModel";
import * as _ from "lodash";
import {
  MemberModel,
  ScheduleModel,
  ServiceCenterModel,
  ServiceCenterStatus,
  ServiceModel
} from "../../../service-center/service-center.model";
@Injectable()
export class SendService extends BaseService {
  constructor(private http: HttpClient) {
    super();
  }

  addReserve(reserve: reserve) {
    console.log("1:", reserve.code1);
    console.log("2:", reserve.code2);
    console.log("3:", reserve.code3);
    console.log("ic:", reserve.iranCode);
    return this.http
      .post(this._apiUrl.reserve, {
        firstname: reserve.firstname,
        lastname: reserve.lastname,
        model: reserve.model,
        lpn: reserve.code1 + reserve.code2 + reserve.code3 + reserve.iranCode,
        code1: reserve.code1,
        code2: reserve.code2,
        code3: reserve.code3,
        iranCode: reserve.iranCode,
        date: reserve.date,
        time: reserve.time,
        mobile: reserve.mobile,
        service: reserve.service,
        voucher: reserve.voucher,
        description: reserve.description,
        //   latitude
        //   longitude     :reserve.
        providerId: reserve.providerId
      }).pipe(
      map((res: any) => {
        return new ValidationResponse(res.message);
      }));
  }

  addServiceCenter(providerId: string): Observable<ServiceCenterModel> {
    console.log("providerId:", providerId);

    return this.http
      .get(this._apiUrl.serviceCenterRUD({ id: providerId })).pipe(
      map((res: any) => {
        let services = [];
        if (res.services && _.isArray(res.services)) {
          services = res.services.pipe(map( ({s}) => {
            return new ServiceModel({
              category: {
                id: s.category,
                title: s.category
              },
              description: s.description,
              id: s.id,
              isMobile: false,
              price: s.price,
              title: s.name,
              company: {
                id: "",
                title: "-",
                company: {
                  id: "",
                  title: "-",
                  icon: "-",
                  image: "-"
                }
              }
            });
          }) );
        }

        return new ServiceCenterModel({
          title: res.name,
          mobile: res.mobile,
          services: services,
          address: {
            country: 'iran',
            city: null,
            address: res.address
          },
        });
      }));
  }
}
