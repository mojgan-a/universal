import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FixComponent } from './fix/fix.component';
import { SendComponent } from './fix/send/send.component';
import { ServiceCenterListComponent } from './request-list/service-center-list/service-center-list.component';
import { UserListComponent } from './request-list/user-list/user-list.component';
import { RequestComponent } from './request.component';
import { ReciveProviderComponent } from './sms/recive-provider/recive-provider.component';
import { ReportComponent } from './sms/report/report.component';
import { SelectServiceComponent } from './sms/select-service/select-service.component';
import { SmsComponent } from './sms/sms.component';
import { UserLocationComponent } from './sms/user-location/user-location.component';
import { WaitingComponent } from './sms/waiting/waiting.component';
import { AuthGuard } from '../../core/auth.guard';

export const routes: Routes = [
    {
        path: '',
        component: RequestComponent,
        children: [
            {
                path: 'sms',
                component: SmsComponent,
                children: [
                    {
                        path: 'recive-provider',
                        component: ReciveProviderComponent,
                        canActivate: [AuthGuard]
                    },
                    {
                        path: 'report',
                        component: ReportComponent,
                        canActivate: [AuthGuard]
                    },
                    {
                        path: 'select-service',
                        component: SelectServiceComponent,
                        canActivate: [AuthGuard]
                    },
                    {
                        path: 'user-location',
                        component: UserLocationComponent
                    },
                    {
                        path: 'waiting',
                        component: WaitingComponent,
                        canActivate: [AuthGuard]
                    },
                    { path: '', redirectTo: 'user-location', pathMatch: 'full',canActivate: [AuthGuard] }
                ],
                // canActivate: [AuthGuard]
            },
            {
                path: 'fix',
                component: FixComponent,
                children: [
                    {
                        path: 'send/:id',
                        component: SendComponent
                    }
                ]
            },
            {
                path: 'list',
                component: UserListComponent
            },
            {
                path: 'provider-list/:id',
                component: ServiceCenterListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
