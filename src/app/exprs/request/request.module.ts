import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DpDatePickerModule } from 'ng2-jalali-date-picker';
import { WidgetsModule } from '../../widgets/widgets.module';
import { FixComponent } from './fix/fix.component';
import { SendComponent } from './fix/send/send.component';
import { SendService } from './fix/send/send.service';
import { ServiceCenterListComponent } from './request-list/service-center-list/service-center-list.component';
import { UserListComponent } from './request-list/user-list/user-list.component';
import { RequestComponent } from './request.component';
import { routing } from './request.routing';
import { ReciveProviderComponent } from './sms/recive-provider/recive-provider.component';
import { ReportComponent } from './sms/report/report.component';
import { SelectServiceComponent } from './sms/select-service/select-service.component';
import { SmsComponent } from './sms/sms.component';
import { UserLocationComponent } from './sms/user-location/user-location.component';
import { WaitingComponent } from './sms/waiting/waiting.component';
import { UserLocationService } from './sms/user-location/user-location.service';

@NgModule({
    imports: [
        CommonModule,
        WidgetsModule,
        FormsModule,
        ReactiveFormsModule,
        DpDatePickerModule,
        routing,
        ReactiveFormsModule
    ],
    declarations: [
        RequestComponent,
        UserLocationComponent,
        SelectServiceComponent,
        WaitingComponent,
        ReciveProviderComponent,
        ReportComponent,
        SmsComponent,
        FixComponent,
        SendComponent,
        ServiceCenterListComponent,
        UserListComponent
    ],
    providers: [SendService,UserLocationService]
})
export class RequestModule {}
