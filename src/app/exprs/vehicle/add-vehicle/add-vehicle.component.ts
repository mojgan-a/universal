
import {filter} from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ActivationEnd } from '@angular/router';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-add-vehicle',
  templateUrl: './add-vehicle.component.html',
  styleUrls: ['./add-vehicle.component.scss']
})
export class AddVehicleComponent implements OnInit {

  public step;
  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
  ) {
    // _route.url.subscribe(() => {
    //   console.log(_route.snapshot.firstChild.data);
    // });
    _router.events.pipe(filter((event) => {
      return event instanceof ActivationEnd && event.snapshot.component === AddVehicleComponent;
    })).subscribe((event: ActivationEnd) => {
      this.step = event.snapshot.firstChild.data.step;
    })
  }

  ngOnInit() {
  }

}
