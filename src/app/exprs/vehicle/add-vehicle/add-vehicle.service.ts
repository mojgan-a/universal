
import {throwError as observableThrowError,  Observable } from 'rxjs';

import {tap, map} from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import * as _ from 'lodash';
import { BaseService } from '../../../core/base-service';
import {
    Car,
    CarModel,
    Color,
    ColorModel,
    Company,
    CompanyModel,
    LPN,
    LPNModel,
    Model,
    ModelModel,
    ValidationResponse
} from '../vehicle.model';

@Injectable()
export class AddVehicleService extends BaseService {
    public companiesRepo: Company[];
    public colorsRepo: Color[];
    public modelsRepo: Model[];
    public carRepo: Car;

    public companiesRepoUpdate = new EventEmitter();
    public colorsRepoUpdate = new EventEmitter();
    public modelsRepoUpdate = new EventEmitter();

    constructor(private http: HttpClient) {
        super();
        this._initRepos();
    }

    getCompanies(): Observable<Company[]> {
        return this.http
            .get(this._apiUrl.vmsBrand).pipe(
            map((res: any) => {
                const result = [];
                res.brands.forEach(company => {
                    result.push(
                        new CompanyModel({
                            title: company.name,
                            id: company.id,
                            icon: '',
                            image: `${this._baseUrl}api/file/avatar/${
                                company.imageId
                            }`
                        })
                    );
                });
                return result;
            }),
            tap(data => {
                this.companiesRepo = data;
                this.companiesRepoUpdate.emit(data);
            }),);
    }

    getCompanyById(id: string) {
        const results = this.companiesRepo.filter(c => {
            return c.id === id;
        });
        return results.length !== 0 ? results[0] : null;
    }

    getModel(): Observable<Model[]> {
        if (_.isNil(this.carRepo.company)) {
            return observableThrowError(Error('company-required'));
        }
        const companyId: string = this.carRepo.company.id;
        return this.http
            .get(this._apiUrl.vmsBrand).pipe(
            map((res: any) => {
                const result = [];
                res.brands
                    .filter(company => {
                        return company.id === companyId;
                    })[0]
                    .models.forEach(model => {
                        result.push(
                            new ModelModel({
                                id: model.id,
                                title: model.name
                            })
                        );
                    });
                return result;
            }),
            tap(data => {
                this.modelsRepo = data;
                this.modelsRepoUpdate.emit(data);
            }),);
    }

    getColors(): Observable<Color[]> {
        return this.http
            .get(this._apiUrl.vmsColor).pipe(
            map((res: any) => {
                const result: Color[] = [];
                res.colors.forEach(color => {
                    result.push(
                        new ColorModel({
                            id: color.id,
                            title: color.name,
                            hex: color.hex
                        })
                    );
                });
                return result;
            }),
            tap(data => {
                this.colorsRepo = data;
                this.colorsRepoUpdate.emit(data);
            }),);
    }

    setCompany(company: Company) {
        if (_.isNil(company)) {
            this.carRepo.company = null;
        } else {
            this.carRepo.company = company;
        }
    }

    setDetails(
        year: number,
        color: Color,
        model: Model,
        lpn: LPN,
        vin: string
    ) {
        if (_.isNil(year)) {
            this.carRepo.year = null;
        } else {
            this.carRepo.year = year;
        }

        if (_.isNil(color)) {
            this.carRepo.color = null;
        } else {
            this.carRepo.color = color;
        }

        if (_.isNil(model)) {
            this.carRepo.model = null;
        } else {
            this.carRepo.model = model;
        }

        if (_.isNil(lpn)) {
            this.carRepo.lpn = null;
        } else {
            this.carRepo.lpn = lpn;
        }

        if (_.isNil(vin)) {
            this.carRepo.vin = null;
        } else {
            this.carRepo.vin = vin;
        }
    }

    hasCompany() {
        return !_.isNil(this.carRepo.company);
    }

    hasDetails() {
        return !(
            _.isNil(this.carRepo.model) &&
            _.isNil(this.carRepo.color) &&
            _.isNil(this.carRepo.year) &&
            _.isNil(this.carRepo.vin) &&
            _.isNil(this.carRepo.lpn)
        );
    }

    pushCar() {
        const lpn = new LPNModel(this.carRepo.lpn).getString();
        console.log(this.carRepo);
        const data = {
            vin: this.carRepo.vin,
            colorId: this.carRepo.color.id.toString(),
            modelId: this.carRepo.model.id.toString(),
            prodYear: Number(this.carRepo.year),
            lpn: lpn
        };
        console.log(data);
        if (this.carRepo.id) {
            return this.http
                .put(`${this._apiUrl.vmsCar}/${this.carRepo.id}`, data).pipe(
                map((res: any) => {
                    return new ValidationResponse(res.message);
                }));
        } else {
            return this.http.post(this._apiUrl.vmsCar, data).pipe(map((res: any) => {
                return new ValidationResponse(res.message);
            }));
        }
    }

    reset() {
        this._initRepos();
    }

    private _initRepos() {
        this.carRepo = new CarModel();
        this.colorsRepo = [];
        this.companiesRepo = [];
        this.modelsRepo = [];
    }
}
