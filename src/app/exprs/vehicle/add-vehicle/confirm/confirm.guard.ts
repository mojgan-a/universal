import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AddVehicleService } from '../add-vehicle.service';
import { ActivatedRoute, Router } from '@angular/router';

@Injectable()
export class ConfirmGuard implements CanActivate {
  constructor (
    private _addVehicleService: AddVehicleService,
    private _router: Router,
    private _route: ActivatedRoute
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    const canGo = this._addVehicleService.hasCompany() && this._addVehicleService.hasDetails();
    if (!canGo) {
      this._router.navigate(['vehicle', 'add-vehicle']);
    }
    return canGo;
  }
}
