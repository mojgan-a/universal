import { Component, OnInit } from '@angular/core';
import { AddVehicleService } from '../add-vehicle.service';
import { Car } from '../../vehicle.model';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {

  constructor(
    private _addVehicleService: AddVehicleService,
    private _router: Router,
    private _route: ActivatedRoute,
  ) { }

  ngOnInit() {
  }

  public next() {
    this._addVehicleService.pushCar().subscribe(() => {
      this._router.navigate(['../../'], {relativeTo: this._route});
    });
  }

  public pre() {
    this._router.navigate(['../details'], {relativeTo: this._route});
  }

  public cancel() {
    this._addVehicleService.reset();
    this._router.navigate(['../../'], {relativeTo: this._route});
  }

  public get car() {
    return this._addVehicleService.carRepo;
  }

}
