import { Component, OnInit } from '@angular/core';
import { AddVehicleService } from '../add-vehicle.service';
import { Company } from '../../vehicle.model';
import { ActivatedRoute, Router } from '@angular/router';

import * as _ from 'lodash';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit {

  public selectedCompanyId: string;

  constructor(
    private _addVehicleService: AddVehicleService,
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit() {
    const repoCompanyId = this._addVehicleService.carRepo.company ? this._addVehicleService.carRepo.company.id : false;
    if (repoCompanyId) {
      this.selectedCompanyId = repoCompanyId;
    } else {
      this.selectedCompanyId = null;
    }
    this._addVehicleService.getCompanies().subscribe();
  }

  public get companies() {
    return this._addVehicleService.companiesRepo;
  }

  public cancel() {
    this._addVehicleService.reset();
    this._router.navigate(['../../'], {relativeTo: this._route});
  }

  public next() {
    const company = this._addVehicleService.getCompanyById(this.selectedCompanyId);
    this._addVehicleService.setCompany(company);
    this._router.navigate(['../details'], {relativeTo: this._route});
  }
}
