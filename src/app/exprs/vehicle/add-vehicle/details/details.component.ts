import { Component, OnInit } from '@angular/core';
import { AddVehicleService } from '../add-vehicle.service';
import { Model, Color, LPN, ColorModel, ModelModel } from '../../vehicle.model';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'jalali-moment';

interface AutoYear {
  jalali: string;
  miladi: string;
}

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  public selectedModel: Model;
  public selectedColor: Color;
  public selectedYear: number;
  public selectedVin: string;
  public selectedLpn: LPN;

  private _yearRange: number = 100;

  constructor(
    private _addVehicleService: AddVehicleService,
    private _router: Router,
    private _route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this._addVehicleService.getColors().subscribe();
    this._addVehicleService.getModel().subscribe();
    this.selectedModel = this._addVehicleService.carRepo.model || null;
    this.selectedColor = this._addVehicleService.carRepo.color || null;
    this.selectedYear = this._addVehicleService.carRepo.year || null;
    this.selectedVin = this._addVehicleService.carRepo.vin || null;
    this.selectedLpn = this._addVehicleService.carRepo.lpn || {
      code1: null,
      code2: null,
      code3: null,
      iranCode: null,
    };
    console.log(this.selectedModel);
  }

  isThisYear(year: AutoYear): boolean {
    return Number(year.jalali) === Number(this.selectedYear);
  }

  isThisColor(color: ColorModel): boolean {
    return this.selectedColor && (color.id === this.selectedColor.id);
  }

  isThisModel(model: ModelModel): boolean {
    return this.selectedModel && (model.id === this.selectedModel.id);
  }

  get years(): AutoYear[] {
    let result = [];
    let i: number;
    let correntDate = moment().endOf('year');
    for (i = 1 ; i < this._yearRange ; i++) {
      result.push({
        jalali: correntDate.format('jYYYY'),
        miladi: correntDate.format('YYYY'),
      })
      correntDate.subtract(1, 'year');
    }
    return result;
  }

  public next() {
    this._addVehicleService.setDetails(
      this.selectedYear,
      this.selectedColor,
      this.selectedModel,
      this.selectedLpn,
      this.selectedVin
    );
    this._router.navigate(['../confirm'], { relativeTo: this._route });
  }

  public pre() {
    this._addVehicleService.setDetails(null, null, null, null, null);
    this._router.navigate(['../company'], { relativeTo: this._route });
  }

  public cancel() {
    this._addVehicleService.reset();
    this._router.navigate(['../../'], { relativeTo: this._route });
  }

  public get colors() {
    return this._addVehicleService.colorsRepo;
  }

  public get models() {
    return this._addVehicleService.modelsRepo;
  }

}
