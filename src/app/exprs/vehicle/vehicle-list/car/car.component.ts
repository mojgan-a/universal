import { Component, OnInit, Input } from '@angular/core';
import { Car } from '../../vehicle.model';
import { AddVehicleService } from '../../add-vehicle/add-vehicle.service';
import { VehicleListService } from '../vehicle-list.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.scss']
})
export class CarComponent implements OnInit {
  @Input() car: Car;
  constructor(
    private _addVehicleService: AddVehicleService,
    private _vehicleListService: VehicleListService,
    private _route: ActivatedRoute,
    private _router: Router,
  ) { }

  ngOnInit() {
  }

  remove() {
    this._vehicleListService.removeCar(this.car).subscribe();
  }

  edite() {
    this._addVehicleService.carRepo = this.car;
    this._router.navigate(['..', 'add-vehicle', 'company'], {relativeTo: this._route});
  }

}
