import { Component, OnInit } from '@angular/core';
import { VehicleListService } from './vehicle-list.service';
import { AddVehicleService } from '../add-vehicle/add-vehicle.service';

@Component({
  selector: 'app-vehicle-list',
  templateUrl: './vehicle-list.component.html',
  styleUrls: ['./vehicle-list.component.scss']
})
export class VehicleListComponent implements OnInit {

  constructor(
    private _vehicleListService: VehicleListService,
    private _addVehicleService: AddVehicleService,
  ) { }

  ngOnInit() {
    this._vehicleListService.getCars().subscribe({
      complete: () => {

      }
    });
  }

  resetCarRepo() {
    this._addVehicleService.reset();
  }

  public get cars() {
    return this._vehicleListService.carsRepo;
  }

}
