
import {tap, map} from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import * as _ from 'lodash';






import { Observable } from 'rxjs';
import { BaseService } from '../../../core/base-service';
import {
    Car,
    CarModel,
    ColorModel,
    CompanyModel,
    LPN,
    LPNModel,
    ModelModel,
    ValidationResponse
} from '../vehicle.model';

@Injectable()
export class VehicleListService extends BaseService {
    public carsRepo: Car[] = [];
    public carRepoUpdate = new EventEmitter();

    constructor(private http: HttpClient) {
        super();
    }

    getCars(): Observable<Car[]> {
        return this.http
            .get(this._apiUrl.vmsCar).pipe(
            map((res: any) => {
                const result = [];
                res.cars.forEach(car => {
                    const company = new CompanyModel({
                        title: car.brand.name,
                        id: car.brand.id,
                        icon: '',
                        image:
                            this._baseUrl +
                            'api/file/avatar/' +
                            car.brand.imageId
                    });
                    const model = new ModelModel({
                        id: car.model.id,
                        title: car.model.name
                    });
                    const color = new ColorModel({
                        id: car.color.id,
                        title: car.color.name,
                        hex: car.color.hex
                    });
                    let lpn: LPN = {
                        code1: car.lpn.slice(0, 2),
                        code2: car.lpn.slice(2, 3),
                        code3: car.lpn.slice(3, 6),
                        iranCode: car.lpn.slice(6, 8)
                    };
                    lpn = new LPNModel(lpn);
                    result.push(
                        new CarModel({
                            id: car.id,
                            company: company,
                            model: model,
                            color: color,
                            year: car.prodYear,
                            lpn: lpn,
                            vin: car.vin
                        })
                    );
                });
                return result;
            }),
            tap(cars => {
                this.carRepoUpdate.emit(cars);
                this.carsRepo = cars;
            }),);
    }

    removeCar(car: Car) {
        return this.http
            .delete(`${this._apiUrl.vmsCar}/${car.id}`).pipe(
            map((res: any) => {
                _.remove(this.carsRepo, item => {
                    return item.id === car.id;
                });
                return new ValidationResponse('');
            }));
    }
}
