import * as _ from 'lodash';
export * from '../../core/general.model';

export interface LPN {
    code1?: string;
    code2?: string;
    code3?: string;
    iranCode?: string;
}

export class LPNModel implements LPN {
    public code1: string;
    public code2: string;
    public code3: string;
    public iranCode: string;
    constructor(obj?: LPN) {
        _.assign(this, obj);
    }

    getString() {
        return (
            this.code1.toString() +
            this.code2.toString() +
            this.code3.toString() +
            this.iranCode.toString()
        );
    }
}

export interface Car {
    id?: string;
    company?: Company;
    model?: Model;
    color?: Color;
    year?: number;
    lpn?: LPN;
    vin?: string;
}

export class CarModel implements Car {
    public id: string;
    public company: Company;
    public model: Model;
    public color: Color;
    public year: number;
    public lpn: LPN;
    public vin: string;

    constructor(obj?: Car) {
        _.assign(this, obj);
    }
}

export interface Company {
    title?: string;
    id?: string;
    icon?: string;
    image?: string;
}

export class CompanyModel implements Company {
    public title: string;
    public id: string;
    public icon: string;
    public image: string;

    constructor(obj?: Company) {
        _.assign(this, obj);
    }
}

export interface Color {
    id?: string;
    title?: string;
    hex?: string;
}

export class ColorModel implements Color {
    public id: string;
    public title: string;
    public hex: string;

    constructor(obj?: Color) {
        _.assign(this, obj);
    }
}

export interface Model {
    id?: string;
    title?: string;
}

export class ModelModel implements Model {
    public id: string;
    public title: string;

    constructor(obj?: Model) {
        _.assign(this, obj);
    }
}

export interface BrandModel {
    id?: string;
    title?: string;
    company?: {
        title?: string;
        id?: string;
        icon?: string;
        image?: string;
    };
}

export class ModelBrandModel implements BrandModel {
    id?: string;
    title?: string;
    company?: {
        title?: string;
        id?: string;
        icon?: string;
        image?: string;
    };

    constructor(obj?: BrandModel) {
        _.assign(this, obj);
    }
}
