import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { routing } from './vehicle.routing';

import { WidgetsModule } from '../../widgets/widgets.module';

import { VehicleComponent } from './vehicle.component';
import { VehicleListComponent } from './vehicle-list/vehicle-list.component';
import { CompanyComponent } from './add-vehicle/company/company.component';
import { ConfirmComponent } from './add-vehicle/confirm/confirm.component';
import { DetailsComponent } from './add-vehicle/details/details.component';
import { AddVehicleComponent } from './add-vehicle/add-vehicle.component';
import { CarComponent } from './vehicle-list/car/car.component';

import { AddVehicleService } from './add-vehicle/add-vehicle.service';
import { VehicleListService } from './vehicle-list/vehicle-list.service';

import { ConfirmGuard } from './add-vehicle/confirm/confirm.guard';
import { DetailsGuard } from './add-vehicle/details/details.guard';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    WidgetsModule,
    routing
  ],
  declarations: [
    VehicleComponent,
    VehicleListComponent,
    CompanyComponent,
    ConfirmComponent,
    DetailsComponent,
    AddVehicleComponent,
    CarComponent
  ],
  providers: [
    AddVehicleService,
    VehicleListService,
    ConfirmGuard,
    DetailsGuard,
  ]
})
export class VehicleModule { }
