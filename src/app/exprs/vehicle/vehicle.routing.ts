import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { VehicleComponent } from './vehicle.component';

import { VehicleListComponent } from './vehicle-list/vehicle-list.component';
import { CompanyComponent } from './add-vehicle/company/company.component';
import { ConfirmComponent } from './add-vehicle/confirm/confirm.component';
import { DetailsComponent } from './add-vehicle/details/details.component';
import { AddVehicleComponent } from './add-vehicle/add-vehicle.component';

import { ConfirmGuard } from './add-vehicle/confirm/confirm.guard';
import { DetailsGuard } from './add-vehicle/details/details.guard';

export const routes: Routes = [
    {
      path: '',
      component: VehicleComponent,
      children: [
        {
          path: 'vehicle-list',
          component: VehicleListComponent
        },
        {
          path: 'add-vehicle',
          component: AddVehicleComponent,
          children: [
            {
              path: 'company',
              component: CompanyComponent,
              data: {
                step: 1,
              }
            },
            {
              path: 'confirm',
              component: ConfirmComponent,
              data: {
                step: 3,
              },
              canActivate: [ConfirmGuard]
            },
            {
              path: 'details',
              component: DetailsComponent,
              data: {
                step: 2,
              },
              canActivate: [DetailsGuard]
            },
            {
              path: '',
              redirectTo: 'company',
              pathMatch: 'full'
            }
          ]
        },
        { path: '', redirectTo: 'vehicle-list', pathMatch: 'full' }
      ]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
