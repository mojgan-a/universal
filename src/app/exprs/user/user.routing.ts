import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { UserComponent } from './user.component';
import { ProfileComponent } from './profile/profile.component';
import { LoginComponent } from './login/login.component';
import { UsernameComponent } from './login/username/username.component';
import { PasswordComponent } from './login/password/password.component';
import { RegisterComponent } from './register/register.component';
import { ConfirmComponent } from './register/confirm/confirm.component';
import { FormComponent } from './register/form/form.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { EditComponent } from './profile/edit/edit.component';
import { InfoComponent } from './profile/info/info.component';
import { AuthGuard } from '../../core/auth.guard';

export const routes: Routes = [
    {
      path: '',
      component: UserComponent,
      children: [
        {
          path: 'profile',
          component: ProfileComponent,
          canActivate: [AuthGuard],
          children: [
            {
              path: 'edit',
              component: EditComponent
            },
            {
              path: 'info',
              component: InfoComponent
            },
            {path: '', redirectTo: 'info', pathMatch: 'full'}
          ]
        },
        {
          path: 'forget-password',
          component: ForgetPasswordComponent,
          data: {
            layout: {
              header: false,
              aside: false,
              footer: false
            }
          }
        },
        {
          path: 'login',
          component: LoginComponent,
          children: [
            {
              path: 'username',
              component: UsernameComponent,
              data: {
                layout: {
                  header: false,
                  aside: false,
                  footer: true
                }
              }
            },
            {
              path: 'password',
              component: PasswordComponent,
              data: {
                layout: {
                  header: false,
                  aside: false,
                  footer: true
                }
              }
            },
            { path: '', redirectTo: 'username', pathMatch: 'full' },
          ]
        },
        {
          path: 'register',
          component: RegisterComponent,
          children: [
            {
              path: 'form',
              component: FormComponent,
              data: {
                layout: {
                  header: false,
                  aside: false,
                  footer: true
                }
              }
            },
            {
              path: 'confirm/:phone',
              component: ConfirmComponent,
              data: {
                layout: {
                  header: false,
                  aside: false,
                  footer: true
                }
              }
            },
            { path: '', redirectTo: 'form', pathMatch: 'full' },
          ]
        },
        { path: '', redirectTo: 'profile', pathMatch: 'full' },
      ]
    },
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
