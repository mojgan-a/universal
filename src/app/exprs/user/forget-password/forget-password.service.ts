
import {catchError, map} from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';






import { Observable } from 'rxjs';
import { AuthService } from '../../../core/auth.service';
import { BaseService } from '../../../core/base-service';
import { UserModel, ValidationResponse } from '../user.model';

@Injectable()
export class ForgetPasswordService extends BaseService {
    public userRepo: UserModel;

    constructor(
        private http: HttpClient,
        private auth: AuthService,
        private router: Router,
        private route: ActivatedRoute
    ) {
        super();
        this._initRepo();
    }

    changePassword(key: string): Observable<ValidationResponse> {
        return this.http
            .post(this._apiUrl.passwordReset, {
                userId: this.userRepo.id,
                key: key,
                password: this.userRepo.password
            }).pipe(
            map((res: any) => {
                this.router.navigate(['/']);
                return new ValidationResponse(res.message);
            }),
            catchError(this._catchBadResponse),);
    }

    sendKey(): Observable<ValidationResponse> {
        return this.http
            .post(this._apiUrl.passwordForget, {
                mobile: this.userRepo.phoneNumber
            }).pipe(
            map((res: any) => {
                return new ValidationResponse(res.message);
            }),
            catchError(this._catchBadResponse),);
    }

    private _initRepo() {
        this.userRepo = new UserModel({
            phoneNumber: '',
            firstName: '',
            password: ''
        });
    }
}
