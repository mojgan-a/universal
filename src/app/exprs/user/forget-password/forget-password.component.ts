import { Component, OnInit } from '@angular/core';
import { ValidationResponse } from '../user.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ForgetPasswordService } from './forget-password.service';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.scss']
})
export class ForgetPasswordComponent implements OnInit {
  public validation: ValidationResponse;
  public form: FormGroup;
  public wait = false;
  public delayTimeout;
  constructor(
    private _fb: FormBuilder,
    private _forgetPasswordService: ForgetPasswordService,
    private _router: Router,
    private _route: ActivatedRoute
  ) { }

  ngOnInit() {
    this._forgetPasswordService.userRepo.id = this._route.snapshot.queryParamMap.get('id');
    this._forgetPasswordService.userRepo.phoneNumber = this._route.snapshot.queryParamMap.get('phone');
    this.validation = new ValidationResponse('');
    this.sendAgain();
    this.form  = this._fb.group({
      code: ['' , Validators.compose([Validators.required, Validators.minLength(5)])],
      password: ['' , Validators.compose([Validators.required])],
    });
    this._wait();
  }

  changePassword(event) {
    if (this.form.valid) {
      const values = this.form.value;
      this._forgetPasswordService.userRepo.password = values.password;
      this._forgetPasswordService.changePassword(values.code).subscribe((res) => {
        this.validation = res;
      });
    }
  }

  private _wait() {
    clearTimeout(this.delayTimeout);
    this.wait = true;
    this.delayTimeout = setTimeout(() => {
      this.wait = false;
    }, 6000);
  }

  sendAgain() {
    if (this.wait) {
      return;
    }
    this._wait();
    this._forgetPasswordService.sendKey().subscribe(res => {
      this.validation = res;
    });

  }

}
