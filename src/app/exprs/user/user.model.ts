import * as g from '../../core/general.model';
import * as _ from 'lodash';
export * from '../../core/general.model';

export interface User {
  phoneNumber: String;
  firstName: String;
  image?: String;
  lastName?: String;
  gender?: String;
  national?: String;
  nationalId?: String;
  address?: g.Address;
  location?: [number, number];
  email?: string;
  password?: String;
  id?: String;
}

export class UserModel implements User {
  phoneNumber: string;
  firstName: string;
  image?: string;
  lastName?: string;
  email?: string;
  gender?: string;
  national?: string;
  nationalId?: string;
  address?: g.Address;
  location?: [number, number];
  password?: string;
  id?: string;

  constructor(obj?: User) {
    _.assign(this, obj);
  }
}
