import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { routing } from './user.routing';
import { WidgetsModule } from '../../widgets/widgets.module';

import { RegisterService } from './register/register.service';
import { LoginService } from './login/login.service';
import { ForgetPasswordService } from './forget-password/forget-password.service';
import { ProfileService } from './profile/profile.service';

import { UserComponent } from './user.component';
import { ProfileComponent } from './profile/profile.component';
import { LoginComponent } from './login/login.component';
import { UsernameComponent } from './login/username/username.component';
import { PasswordComponent } from './login/password/password.component';
import { RegisterComponent } from './register/register.component';
import { ConfirmComponent } from './register/confirm/confirm.component';
import { FormComponent } from './register/form/form.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { EditComponent } from './profile/edit/edit.component';
import { InfoComponent } from './profile/info/info.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    WidgetsModule,
    routing
  ],
  providers: [
    RegisterService,
    LoginService,
    ForgetPasswordService,
    ProfileService,
  ],
  declarations: [
    UserComponent,
    ProfileComponent,
    LoginComponent,
    UsernameComponent,
    PasswordComponent,
    RegisterComponent,
    ConfirmComponent,
    FormComponent,
    ForgetPasswordComponent,
    EditComponent,
    InfoComponent,
  ]
})
export class UserModule { }
