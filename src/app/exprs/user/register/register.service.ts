
import {catchError, map} from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';





import { Observable } from 'rxjs';
import { AuthService } from '../../../core/auth.service';
import { BaseService } from '../../../core/base-service';
import { User, UserModel, ValidationResponse } from '../user.model';

@Injectable()
export class RegisterService extends BaseService {
    public userRepo: UserModel;

    constructor(
        private http: HttpClient,
        private auth: AuthService,
        private router: Router
    ) {
        super();
        this.initRepo();
    }

    register(user: User): Observable<ValidationResponse> {
        return this.http
            .post(this._apiUrl.register, {
                mobile: user.phoneNumber,
                firstName: user.firstName,
                password: user.password
            }).pipe(
            map(response => {
                return new ValidationResponse(response['message']);
            }),
            catchError(this._catchBadResponse),);
    }

    initRepo() {
        this.userRepo = new UserModel({
            phoneNumber: '',
            firstName: '',
            password: ''
        });
    }

    confirm(phone: string, code: string): Observable<ValidationResponse> {
        return this.http
            .post(this._apiUrl.confirm, {
                mobile: phone,
                key: code
            }).pipe(
            map((response: any) => {
                this.auth.login(response.token, {
                    id: response.userId,
                    firstName: response.firstName,
                    lastName: response.lastName,
                    imageId: response.imageId,
                    phoneNumber: response.phoneNumber
                });
                this.router.navigate(['/']);
                return new ValidationResponse(response['message']);
            }),
            catchError(this._catchBadResponse),);
    }

    sendSMSSecretKey(phone) {
        return this.http
            .post(this._apiUrl.smsSecretKey, {
                mobile: phone
            }).pipe(
            map((res: any) => {
                return new ValidationResponse(res.message);
            }),
            catchError(this._catchBadResponse),);
    }
}
