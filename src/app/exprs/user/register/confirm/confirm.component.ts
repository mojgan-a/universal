import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RegisterService } from '../register.service';
import { ValidationResponse } from '../../user.model';
import { ToastsManager } from 'ng6-toastr';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {
  public code = ['', '', '', '', ''];
  public phone: string;
  public canReloadMessage: Boolean = true;
  public validation: ValidationResponse;

  @ViewChild('code1') code1: any;
  @ViewChild('code2') code2: any;
  @ViewChild('code3') code3: any;
  @ViewChild('code4') code4: any;
  @ViewChild('code5') code5: any;
  @ViewChild('submitBtn') submitBtn: any;

  constructor(
    private _registerService: RegisterService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _tost: ToastsManager,
    private _vcr: ViewContainerRef
  ) {
    this._tost.setRootViewContainerRef(_vcr);
  }

  ngOnInit() {
    this._route.params.subscribe(params => {
      this.phone = params.phone;
    });
  }

  onSubmit() {
    const code = this.code.join('');
    this.code = ['', '', '', '', '']
    this._registerService.confirm( this.phone, code).subscribe(
      result => {
        this.validation = result;
        if (!result.ok) {
          this.code1.nativeElement.focus();
        } else {
          this._tost.success('با موفقیت ثبت نام شدید.');
          setTimeout(() => {
            this._router.navigate(['/']);
          }, 1000);
        }
      },
      err => {
        this._tost.error(err);
        console.error(err);
      }
    );
  }

  sendSms() {
    this.canReloadMessage = false;
    this._registerService.sendSMSSecretKey(this.phone).subscribe(
      result => {
        if (result.ok) {
          this._tost.success('کد جدید ارسال شد.');
        } else {
          this._tost.error('مشکل در ارسال از سمت سرور');
        }
      },
      err => {
        this._tost.error('خطا!');
        console.error(err);
      },
      () => {
        setTimeout(() => {
          this.canReloadMessage = true;
        }, 2000);
      }
    );
  }

  keytab(event, inputIndex) {
    if (event.target.value.length == 1) {
      switch (inputIndex) {
        case 2:
          this.code2.nativeElement.focus();
          break;
        case 3:
          this.code3.nativeElement.focus();
          break;
        case 4:
          this.code4.nativeElement.focus();
          break;
        case 5:
          this.code5.nativeElement.focus();
          break;
        case 6:
          this.submitBtn.nativeElement.focus();
          break;
        default:
          this.code1.n_tostativeElement.focus();
      }
    }
  }
}
