import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RegisterService } from '../register.service';
import { ToastsManager } from 'ng6-toastr';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  public registerForm: FormGroup;
  public registerFormError: string;

  constructor(
    private _fb: FormBuilder,
    private _registerService: RegisterService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _tost: ToastsManager,
    private _vcr: ViewContainerRef,
  ) {
    this._tost.setRootViewContainerRef(_vcr);
  }

  ngOnInit() {
    const user = this._registerService.userRepo;
    this.registerForm = this._fb.group({
      username: [user.phoneNumber, Validators.compose([Validators.required, Validators.minLength(10)])],
      name: [user.firstName, Validators.required],
      password: [user.password, Validators.compose([Validators.required, Validators.minLength(6)])],
    });
  }

  onSubmit(event) {
    if (this.registerForm.valid) {
      const value = this.registerForm.value;
      this._registerService.register({
        phoneNumber: value.username,
        firstName: value.name,
        password: value.password
      }).subscribe(result => {
        if (result.ok) {
          this._router.navigate(['../confirm/', value.username], { relativeTo: this._route });
        } else {
          this._tost.error('خطا در سرور!')
        }
      }, err => {
          this._tost.error('خطا در سیستم!')
      });
    }
  }

}
