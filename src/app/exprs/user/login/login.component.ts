import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { carAnimate } from './car.animate';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: carAnimate,
})
export class LoginComponent implements OnInit {

  public car = {
    position: 'first',
    lift: 'inactive'
  };

  constructor(
    private _loginService: LoginService,
    private _router: Router,
    private _route: ActivatedRoute
  ) { }

  public animationDoneFn(event) {
    if (event.triggerName === "car2Position" && event.fromState === "in" && event.toState === "out") {
      this._router.navigateByUrl('/');
    }
  }

  private _goToAnimateStep(step: number): void {
    switch (step) {
      case 1:
        if (this.car.position !== 'in') {
          this.car.position = 'first';
          setTimeout(() => {
            this.car.position = 'in';
          }, 1);
        }
        if (this.car.lift !== 'inactive') {
          setTimeout(() => {
            this.car.lift = 'inactive';
          }, 1)
        }
        break;
      case 2:
        if (this.car.position !== 'in') {
          this.car.position = 'in';
        }
        if (this.car.lift !== 'active') {
          this.car.lift = 'inactive';
          this.car.lift = 'active';
        }
        break;
      case 3:
        this.car.position = 'out';
        break;
    }
  }

  ngOnInit() {
    this._loginService.animation.goStep1.subscribe(() => {
      this._goToAnimateStep(1);
    })

    this._loginService.animation.goStep2.subscribe(() => {
      this._goToAnimateStep(2);
    })

    this._loginService.animation.goStep3.subscribe(() => {
      this._goToAnimateStep(3);
    })
  }

}
