
import {fromEvent as observableFromEvent,  Observable ,  Subscription } from 'rxjs';

import {debounceTime, map} from 'rxjs/operators';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
// import { Ng2DeviceService } from 'ng2-device-detector';
import { ValidationResponse } from '../../user.model';
import { LoginService } from '../login.service';

@Component({
    selector: 'app-username',
    templateUrl: './username.component.html',
    styleUrls: ['./username.component.scss']
})
export class UsernameComponent implements OnInit, OnDestroy {
    public marginTop = '0px';
    public username: string;
    public validation: ValidationResponse;
    private valueChange: Observable<any>;
    private valueChangeSubscription: Subscription;
    constructor(
        private _loginService: LoginService,
        private _router: Router,
        private _route: ActivatedRoute,
        // private _device: Ng2DeviceService
    ) {}

    ngOnInit() {
        this.validation = new ValidationResponse('');
        this.username = this._loginService.userRepo.phoneNumber;
        this.valueChange = observableFromEvent(
            document.getElementById('phone-number'),
            'keyup'
        ).pipe(map((x: any) => x.currentTarget.value));
        this.valueChangeSubscription = this.valueChange.pipe(
            debounceTime(1000))
            .subscribe(value => this.check(value));
        this._loginService.animation.goStep1.emit();
    }

    onSubmit() {
        if (this.validation.ok) {
            this._loginService.userRepo.phoneNumber = this.username;
            this._router.navigate(['../password'], {
                relativeTo: this._route,
                queryParams: {
                    id: this._loginService.userRepo.id,
                    username: this.username
                }
            });
            this._loginService.animation.goStep2.emit();
        }
    }

    check(value) {
        this._loginService.checkUserName(value).subscribe(
            result => {
                this.validation = result;
            },
            err => {
                console.error(err);
                alert(err);
            }
        );
    }

    // collapsForInput() {
    //     if (!this._device.isMobile()) {
    //         return;
    //     }
    //     this.marginTop = '-80px';
    // }

    // unCollapsForInput() {
    //     if (!this._device.isMobile()) {
    //         return;
    //     }
    //     this.marginTop = '0px';
    // }

    ngOnDestroy() {
        this.valueChangeSubscription.unsubscribe();
    }
}
