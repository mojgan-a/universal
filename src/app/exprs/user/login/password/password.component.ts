import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ValidationResponse } from '../../user.model';
import { LoginService } from '../login.service';

@Component({
    selector: 'app-password',
    templateUrl: './password.component.html',
    styleUrls: ['./password.component.scss']
})
export class PasswordComponent implements OnInit {
    public userId: string;
    public username: string;
    public password: string;
    public validation: ValidationResponse;

    constructor(
        private _loginService: LoginService,
        private _route: ActivatedRoute,
        private _router: Router
    ) {}

    ngOnInit() {
        this._loginService.animation.goStep2.emit();
        this.validation = new ValidationResponse('');
        this.password = '';
        this._route.queryParams.subscribe(params => {
            this.userId = params.id;
            this.username = params.username;
            if (!params.username && !params.id) {
                this._router.navigate(['../../username'], {
                    relativeTo: this._route
                });
            }
        });
    }

    onSubmit() {
        this._loginService.userRepo.phoneNumber = this.username;
        this._loginService.userRepo.password = this.password;
        this._loginService.login().subscribe(
            result => {
                this.validation = result;
                if (result.ok) {
                    this._loginService.animation.goStep3.emit();
                }
            },
            err => {
                console.error(err);
                alert('error!');
            }
        );
    }
}
