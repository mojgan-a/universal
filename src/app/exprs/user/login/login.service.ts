
import {catchError, map} from 'rxjs/operators';
import { Injectable, EventEmitter } from '@angular/core';
import { ValidationResponse, UserModel } from '../user.model';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../../core/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseService } from '../../../core/base-service';

import { Observable } from 'rxjs';







@Injectable()
export class LoginService extends BaseService {
  public animation = {
    goStep1: new EventEmitter(),
    goStep2: new EventEmitter(),
    goStep3: new EventEmitter(),
  }
  public userRepo: UserModel;

  constructor(
    private http: HttpClient,
    private auth: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    super();
    this._initRepo();
  }

  checkUserName(username: string): Observable<ValidationResponse> {
    return this.http.post(this._apiUrl.checkUserName, {
      username: username
    }).pipe(map((res: any) => {
      this.userRepo.id = res.userId;
      this.userRepo.phoneNumber = res.username;
      return new ValidationResponse(res.message);
    }),
    catchError(this._catchBadResponse),);
  }

  login(): Observable<ValidationResponse> {
    return this.http.post(this._apiUrl.login, {
      username: this.userRepo.phoneNumber,
      password: this.userRepo.password
    }).pipe(map((res: any) => {
      this.auth.login(res.token, {
        id: res.userId,
        phoneNumber: this.userRepo.phoneNumber,
        firstName: res.firstName,
        lastName: res.lastName,
        imageId: res.imageId,
      });
      this.animation.goStep3.emit();
      // this.router.navigate(['/']);
      return new ValidationResponse('');
    }),catchError(this._catchBadResponse),);
  }

  private _initRepo() {
    this.userRepo = new UserModel({
      phoneNumber: '',
      firstName: '',
      password: '',
    });
  }
}
