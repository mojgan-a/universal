import {
    trigger,
    state,
    style,
    animate,
    transition,
    query,
    animateChild,
    group
  } from '@angular/animations';

export const carAnimate = [
    trigger('car1Position', [
      state('first', style({
        left: '-300px',
      })),
      state('in', style({
        left: 'calc(50% + 10px)',
      })),
      state('out', style({
        left: 'calc(100% + 500px)',
      })),
      transition('first => in', group([
        animate('2s ease-out'),
        query('.wheel', [
          style({
            transform: 'rotate(0deg)'
          }),
          animate('2s ease-out', style({
            transform: 'rotate(1800deg)'
          }))
        ]),
      ])),
      transition('in => out', group([
        query('.wheel', [
          style({
            transform: 'rotate(0deg)'
          }),
          animate('2s ease-in', style({
            transform: 'rotate(1800deg)'
          }))
        ]),
        animate('2s ease-in'),
      ]))
    ]),
    trigger('car2Position', [
      state('in', style({
        left: 'calc(50% - 190px)',
      })),
      state('out', style({
        left: 'calc(100% + 300px)',
      })),
      transition('in => out', group([
        query('.wheel-back', [
          style({
            transform: 'rotate(0deg)'
          }),
          animate('2s ease-in', style({
            transform: 'rotate(1800deg)'
          }))
        ]),
        animate('2s ease-in'),
      ])),
    ]),
    trigger('anchor', [
      state('inactive', style({
        transform: 'rotate(0deg)',
        'transform-origin': 'top'
      })),
      state('active', style({
        transform: 'rotate(20deg)',
        'transform-origin': 'top'
      })),
      transition('inactive <=> active', animate('1s ease-in-out'))
    ]),
    trigger('liftCar', [
      state('inactive', style({
        transform: 'rotate(0deg)',
        'transform-origin': 'top'
      })),
      state('active', style({
        transform: 'rotate(-2deg)',
        'transform-origin': 'top'
      })),
      transition('inactive <=> active', animate('1s 700ms ease-out'))
    ])
  ];