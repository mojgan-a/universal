
import {of as observableOf,  Observable } from 'rxjs';

import {map, catchError, tap} from 'rxjs/operators';
import { Injectable, EventEmitter } from '@angular/core';
import { UserModel, ValidationResponse } from '../user.model';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../../core/auth.service';
import { BaseService } from '../../../core/base-service';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../../../environments/environment';







interface UserProfile {
  firstName: string;
  lastName: string;
  nationalId: string;
  gender: 'man' | 'woman';
  address: string;
}

@Injectable()
export class ProfileService extends BaseService{
  public userRepo: UserModel;
  public repoUpdate = new EventEmitter();

  constructor(
    private http: HttpClient,
    private auth: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    super();
    this._initRepo();
  }

  getUser(): Observable<UserModel> {
    const currentUser = this.auth.getUser();
    return this.http.get(this._apiUrl.profile).pipe(map((res: any) => {
      const contacts = [];
      const email = res.contacts.filter((con: any) => {
        return con.type === 'email';
      })[0];
      return new UserModel({
        id: currentUser.id,
        phoneNumber: currentUser.phoneNumber,
        firstName: res.firstName,
        lastName: res.lastName,
        nationalId: res.nationalId,
        image: res.imageId,
        email: email,
        password: '',
      });
    }),tap(data => {
      this.userRepo = data;
      this.repoUpdate.emit(data);
      return data;
    }),);
  }

  updateProfile(profile: UserProfile): Observable<ValidationResponse> {
    return this.http.post(this._apiUrl.profile, {
      firstName: profile.firstName,
      lastName: profile.lastName,
      nationalId: profile.nationalId,
      gender: profile.gender,
      address: profile.address
    }).pipe(map((res: any) => {
      return new ValidationResponse(res.message);
    }),catchError(this._catchBadResponse),);
  }

  getImagePath() {
    return this.http.get(this._apiUrl.profile).pipe(map((res: any) => {
      if (res.imageId) {
        return environment.baseUrl + this._apiUrl.getImage({
          id: res.imageId
        }).slice(3);
      } else {
        return this._apiUrl.defaultUserImage;
      }
    }),catchError((err, source) => {
      if (err instanceof Error) {
        console.error(err);
      }
      return observableOf(this._apiUrl.defaultUserImage);
    }),);
  }

  uploadProfilePic(fileToUpload: File): Observable<ValidationResponse> {
    const formData: FormData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    return this.http.post(this._apiUrl.userImage, formData).pipe(map((res: any) => {
      return new ValidationResponse(res.message);
    }));
  }

  private _initRepo() {
    this.userRepo = new UserModel({
      phoneNumber: '',
      firstName: '',
      password: '',
    });
  }
}
