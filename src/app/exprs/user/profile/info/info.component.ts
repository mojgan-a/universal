import { Component, OnInit, Input } from '@angular/core';
import { UserModel } from '../../user.model';
import { ProfileService } from '../profile.service';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {

  constructor(
    private _profileService: ProfileService
  ) { }

  ngOnInit() {
    this._profileService.getUser().subscribe(data => {
      this._profileService.userRepo = data;
    });
  }

  get user() {
    return this._profileService.userRepo;
  }



}
