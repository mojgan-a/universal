import { Component, OnInit, ViewChild, ViewContainerRef, ElementRef, Renderer2 } from '@angular/core';
import { ValidationResponse, UserModel } from '../user.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ProfileService } from './profile.service';
import { ToastsManager } from 'ng6-toastr';


import { Observable } from 'rxjs';



@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  public validation: ValidationResponse;
  public userPic;
  public fileName = '-';
  public loading: boolean = false;
  public openUploadBox: boolean = false;
  public fileSelected = false;
  private _userPicOrginal: string;
  @ViewChild('fileInput') fileInput: ElementRef;
  constructor(
    private _fb: FormBuilder,
    private _profileService: ProfileService,
    private _router: Router,
    private _tost: ToastsManager,
    // private _vcr: ViewContainerRef,
    private _route: ActivatedRoute,
    private _renderer2:Renderer2
  ) {
    // this._tost.setRootViewContainerRef(_vcr);
  }

  ngOnInit() {
    this._getProfileData();
    this._getUserPic();
  }

  public get user() {
    return this._profileService.userRepo;
  }

  public selectFile() {
    // let event = new MouseEvent('click', {bubbles: true});
    // this._renderer2.invokeElementMethod(
    //   this.fileInput.nativeElement, 'dispatchEvent', [event]);
    this.fileInput.nativeElement.click()
  }

  public uploadPic() {
    if (!this.fileSelected) {
      return;
    }
    this.loading = true;
    this._profileService.uploadProfilePic(this.fileInput.nativeElement.files.item(0)).subscribe(res => {
      if(res.ok) {
        this.openUploadBox = false;
        this._tost.success('تغییر عکس پروفایل');
      } else {
        this._tost.error('خطا در سرور!');
      }
    }, (err) => {
      console.error(err)
      this._tost.error('خطا در سیستم!');
    }, () => {
      this.loading = false;
    });
  }

  public handleFileInput(files) {
    if (files[0]) {
      if (files[0].size > 5000000) {
        this._tost.error('سایز فایل انتخابی بیشتر از ۵ مگابایت است!');
        return;
      }
      this.fileSelected = true;
      this.fileName = files[0].name;
      this._getBase64(files[0]).subscribe((urlBase64) => {
        this.userPic = urlBase64;
      })
    } else {
      this.fileSelected = false;
      this.fileName = '-';
      this.userPic = this._userPicOrginal;
    }
  }

  private _getUserPic() {
    this._profileService.getImagePath().subscribe(p => {
      this.userPic = p;
      this._userPicOrginal = p;
    });
  }

  private _getProfileData() {
    this._profileService.getUser().subscribe(data => {
      this._profileService.userRepo = data;
    });
  }

  private _getBase64(file): Observable<any> {
    return Observable.create((observer) => {
      var reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        observer.next(reader.result);
        observer.complete();
      };
      reader.onerror = (error) => {
        observer.error(error);
      };
    })
  }

}
