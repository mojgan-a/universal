import { Component, OnInit } from '@angular/core';
import { ValidationResponse, UserModel } from '../../user.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ProfileService } from '../profile.service';
import { nationality } from './nationality';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  public validation: ValidationResponse;
  public nationalities = nationality;
  public form: FormGroup;
  constructor(
    private _fb: FormBuilder,
    private _profileService: ProfileService,
    private _router: Router,
    private _route: ActivatedRoute
  ) { }

  ngOnInit() {
    this._profileService.repoUpdate.subscribe((data) => {
      this._initForm(data);
    });
    this._initForm(this._profileService.userRepo);
  }

  onSubmit() {
    if (this.form.valid) {
      this._profileService.updateProfile(this.form.value).subscribe(res => {
        if (res.ok) {
          this._router.navigate(['../info'], { relativeTo: this._route });
        }
      });
    }
  }

  private _initForm(data) {
    this.form = this._fb.group({
      firstName: [data.firstName, Validators.required],
      lastName: [data.lastName, Validators.required],
      // gender: [data.gender],
      // nationalId: [data.nationalId],
      // address: [data.address],
    });
  }
}
