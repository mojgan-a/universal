
import {map} from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as _ from 'lodash';
import { Observable } from 'rxjs';
import { BaseService } from '../../../core/base-service';
import { UserModel } from '../../user/user.model';
import {
    MemberModel,
    ScheduleModel,
    ServiceCenterModel,
    ServiceCenterStatus,
    ServiceModel
} from '../service-center.model';

const catList = [
    'CNG تجهیزات',
    'اسپرتی',
    'اگزوز سازی',
    'بوستر سازی',
    'پارکینگ',
    'پالایش و پخش فیلتر و روغن',
    'پمپ سازی',
    'تراشکاری',
    'تعمیرات جعبه فرمان و پمپ هیدرولیک',
    'تون آپ',
    'جلوبندی',
    'رادیاتور سازی',
    'رینگ و لاستیک فروشی',
    'سیم پیچی و باتری سازی',
    'شیشه و آیینه اتومبیل',
    'صافکاری و نقاشی',
    'فروشگاه باتری',
    'کارواش',
    'مکانیکی',
    'لوازم یدکی'
];

@Injectable()
export class SingleService extends BaseService {
    constructor(
        private http: HttpClient,
        private route: ActivatedRoute,
        private router: Router
    ) {
        super();
    }

    getServiceCenter(id): Observable<ServiceCenterModel> {
        return this.http
            .get(this._apiUrl.serviceCenterRUD({ id: id })).pipe(
            map((res: any) => {
                // const result = [];
                // if (_.isNil(res.providers)) {
                //   return result;
                // }
                // res.providers.forEach(sc => {
                //   const services = [];
                //   let image;
                //   if (sc.image) {
                //     image = sc.image
                //   } else {
                //     image = this._getCatImage(sc.categories);
                //   }

                //   let desc;
                //   if (sc.categories && _.isArray(sc.categories)) {
                //     desc = sc.categories.join('-');
                //   } else {
                //     desc = '-';
                //   }

                //   result.push(
                //     new ServiceCenterModel({
                //       id: sc.id,
                //       title: sc.name,
                //       address: sc.address,
                //       category: this._getCatText('mechanic'),
                //       image: image,
                //       markerImage: this._getCatMarkerImage(sc.categories),
                //       description: desc,
                //       mobile: '09xxxxxxxxx',
                //       phoneNumbers: ['021xxxxxxxx'],
                //       location: [sc.latitude, sc.longitude],
                //       members: null,
                //       services: services
                //     })
                //   );
                // });
                // return result[0];
                
                let image;
                if (res.imageId) {
                    image = `${this._baseUrl}api/file/avatar/${res.imageId}`;
                } else {
                    image = 'assets/images/service-center.png';
                }

                let members = [];
                if (res.members && _.isArray(res.members)) {
                    members = res.members.pipe(map( ({m}) => {
                        return new MemberModel({
                            id: m.userId,
                            job: m.job,
                            location: null,
                            user: new UserModel({
                                firstName: m.firstName,
                                lastName: m.lastName,
                                id: m.userId,
                                phoneNumber: ''
                            })
                        });
                    }));
                }

                let services = [];
                if (res.services && _.isArray(res.services)) {
                    services = res.services.pipe(map( ({s}) => {
                        return new ServiceModel({
                            category: {
                                id: s.category,
                                title: s.category
                            },
                            description: s.description,
                            id: s.id,
                            isMobile: false,
                            price: s.price,
                            title: s.name,
                            company: {
                                id: '',
                                title: '-',
                                company: {
                                    id: '',
                                    title: '-',
                                    icon: '-',
                                    image: '-'
                                }
                            }
                        });
                    }));
                }

                let schedule = [];
                if (res.schedule) {
                    schedule = res.schedule.pipe(map( ({s}) => {
                        return new ScheduleModel({
                            weekday: s.weekday,
                            startHour: s.startHour,
                            startMinute: s.startMinute,
                            endHour: s.endHour,
                            endMinute: s.endMinute
                        });
                    }));
                }

                let status = ServiceCenterStatus.Active;
                if (
                    res.attachments &&
                    res.attachments.filter(i => {
                        !i.isConfirmed;
                    }).length !== 0
                ) {
                    status = ServiceCenterStatus.PendingToAccept;
                }

                return new ServiceCenterModel({
                    id: id,
                    ownerId: res.ownerId,
                    title: res.name,
                    slogan: res.advertisement,
                    address: {
                        country: 'iran',
                        city: null,
                        address: res.address
                    },
                    category: res.categories,
                    image: image,
                    markerImage: this._getCatMarkerImage(res.categories),
                    description: res.description,
                    mobile: res.mobile,
                    phoneNumbers: res.phoneNumbers,
                    location: [res.latitude, res.longitude],
                    members: members,
                    services: services,
                    schedule: schedule,
                    background: res.background,
                    status: status
                });
            }));
        // return Observable.create((observer) => {
        //   observer.next([
        //     new ServiceCenterModel({
        //       id: 'id123asd',
        //       ownerId: '5acb1dd4ed3c7743afb1e197',
        //       title: 'title',
        //       slogan: 'ma khoobim ma khoobim',
        //       address: {
        //         city: 'tehran',
        //         country: 'iran',
        //         address: 'asdasd',
        //       },
        //       category: this._getCatText('mechanic'),
        //       image:  this._getCatImage('mechanic'),
        //       markerImage: this._getCatMarkerImage('mechanic'),
        //       description: 'description description description description description description',
        //       mobile: '09xxxxxxxxx',
        //       phoneNumbers: ['021xxxxxxxx', '021xxxxxxxx'],
        //       location: [35.79752231642691, 51.41057375818491],
        //       members: [
        //         new MemberModel({
        //           id: '12h3g41h2g34l',
        //           user: {
        //             id: '12379409712hv34lhglh',
        //             phoneNumber: '0912804654645',
        //             address: {
        //               address: 'asdasd',
        //               city: 'asd',
        //               country: 'asd',
        //               location: [0, 0]
        //             },
        //             email: 'asdasd',
        //             firstName: 'alireza',
        //             lastName: 'molaee',
        //             gender: 'men',
        //             image: 'asdasdasd',
        //             location: [0, 0],
        //             national: 'asd',
        //             nationalId: '123123123',
        //             password: 'asdasd',
        //           },
        //           job: 'asd',
        //           location: [0, 0]
        //         }),
        //         new MemberModel({
        //           id: '12h3g41h2g34l',
        //           user: {
        //             id: '12379409712hv34lhglh',
        //             phoneNumber: '0912804654645',
        //             address: {
        //               address: 'asdasd',
        //               city: 'asd',
        //               country: 'asd',
        //               location: [0, 0]
        //             },
        //             email: 'asdasd',
        //             firstName: 'mamad',
        //             lastName: 'asghari',
        //             gender: 'men',
        //             image: 'asdasdasd',
        //             location: [0, 0],
        //             national: 'asd',
        //             nationalId: '123123123',
        //             password: 'asdasd',
        //           },
        //           job: 'asd',
        //           location: [0, 0]
        //         })
        //       ],
        //       services: [
        //         new ServiceModel({
        //           category: {
        //             id: 'asd',
        //             title: 'mechanic'
        //           },
        //           description: 'asdasdasd',
        //           id: '12g4;123g4k12g34',
        //           isMobile: false,
        //           price: 20000,
        //           title: 'taviz tasme',
        //           company: {
        //             id: '1234g1l2k4',
        //             title: 'benz',
        //             icon: 'benz',
        //             image: 'asd.png'
        //           }
        //         }),
        //         new ServiceModel({
        //           category: {
        //             id: 'asd',
        //             title: 'mechanic'
        //           },
        //           description: 'asdasdasd',
        //           id: '12g4;123g4k12g34',
        //           isMobile: false,
        //           price: 20000,
        //           title: 'taviz tasme',
        //           company: {
        //             id: '1234g1l2k4',
        //             title: 'benz',
        //             icon: 'benz',
        //             image: 'asd.png'
        //           }
        //         }),
        //         new ServiceModel({
        //           category: {
        //             id: 'asd',
        //             title: 'mechanic'
        //           },
        //           description: 'asdasdasd',
        //           id: '12g4;123g4k12g34',
        //           isMobile: false,
        //           price: 20000,
        //           title: 'taviz tasme',
        //           company: {
        //             id: '1234g1l2k4',
        //             title: 'benz',
        //             icon: 'benz',
        //             image: 'asd.png'
        //           }
        //         }),
        //         new ServiceModel({
        //           category: {
        //             id: 'asd',
        //             title: 'mechanic'
        //           },
        //           description: 'asdasdasd',
        //           id: '12g4;123g4k12g34',
        //           isMobile: false,
        //           price: 20000,
        //           title: 'taviz tasme',
        //           company: {
        //             id: '1234g1l2k4',
        //             title: 'benz',
        //             icon: 'benz',
        //             image: 'asd.png'
        //           }
        //         }),
        //       ],
        //       schedule: [
        //         new ScheduleModel({
        //           weekday: 2,
        //           startHour: 7,
        //           startMinute: 0,
        //           endHour: 21,
        //           endMinute: 0
        //         }),
        //         new ScheduleModel({
        //           weekday: 1,
        //           startHour: 7,
        //           startMinute: 0,
        //           endHour: 20,
        //           endMinute: 0
        //         })

        //       ]
        //     })
        //   ])
        // })
    }

    private _getCatMarkerImage(cats): string {
        let cat;
        if (cats && cats[0] && catList.indexOf(cats[0]) > -1) {
            cat = cats[0];
        } else {
            cat = 'default';
        }
        return `assets/images/markers/cat-${cat}.svg`;
    }

    private _getCatText(cat) {
        return 'مکانیکی';
    }
}
