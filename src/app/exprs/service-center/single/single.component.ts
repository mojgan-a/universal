import { Location } from '@angular/common';
import { Component, EventEmitter, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { latLng, layerGroup, LayerGroup, tileLayer,marker,icon } from 'leaflet';
import { AuthService } from '../../../core/auth.service';
import { SingleService } from './single.service';

@Component({
    selector: 'app-single',
    templateUrl: './single.component.html',
    styleUrls: ['./single.component.scss']
})
export class SingleComponent implements OnInit {
    public sc;
    public mapReady = new EventEmitter();

    // Map
    public options = {
        layers: [
            tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                maxZoom: 18,
                attribution: '...'
            })
        ],
        dragging: false
    };
    public layers: LayerGroup = layerGroup([]);
    public zoom = 14;
    public center = latLng(35.800397, 51.4091954);
    constructor(
        public location: Location,
        private _singleService: SingleService,
        private _route: ActivatedRoute,
        private _auth: AuthService
    ) {

    }

    
    onMapReady(map) {
        this.mapReady.emit(map);
        this.layers.addTo(map);
    }


    
   
    ngOnInit() {
        
        this._singleService
            .getServiceCenter(this._route.snapshot.params['id'])
            .subscribe(sc => {
                console.log(sc);
                
                this.center=latLng(sc.location);
                console.log(
                    '[single.component.ngOnInit()]: sc.location:', sc.location);
                sessionStorage.setItem('coordinates', sc.location.toString());

                this.layers.addLayer(
                    marker(sc.location, {
                        icon: icon({
                            iconSize: [45, 45],
                            iconAnchor: [22.5, 45],
                            iconUrl: sc.markerImage
                        })
                    })
                );

                
                // this.mapReady.subscribe(map => {
                //     map.setView(latLng(sc.location[0], sc.location[1]), 14);
                // });
                this.sc = sc;
            });


            
    }

    // get canAccess() {
    //     const currentUser = this._auth.getUser();
    //     return this.sc.ownerId === currentUser.id;
    // }
}
