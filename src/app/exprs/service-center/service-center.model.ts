import * as _ from 'lodash';
import * as g from '../../core/general.model';
import { User } from '../user/user.model';
import { ModelBrandModel } from '../vehicle/vehicle.model';
export * from '../../core/general.model';

export interface Service {
    id?: string;
    title?: string;
    description?: string;
    price?: number;
    referenceId?: number;
    isMobile?: boolean;
    category?: ServiceGroup;
    company?: ModelBrandModel;
}

export class ServiceModel implements Service {
    public id: string;
    public title: string;
    public description: string;
    public price: number;
    public isMobile: boolean;
    public category: ServiceGroup;
    public company: ModelBrandModel;
    public referenceId: number;
    constructor(obj: Service) {
        _.assign(this, obj);
    }
}

export interface ServiceGroup {
    id?: string;
    title?: string;
}

export class ServiceGroupModel implements ServiceGroup {
    public id: string;
    public title: string;

    constructor(obj: ServiceGroup) {
        _.assign(this, obj);
    }
}

export interface ServiceCenterFilter {
    listId?: string[];
    category?: string;
    servicesId?: string[];
    name?: string;
    latitude?: number;
    longitude?: number;
    scope?: number;
    isMobile?: boolean;
    skip?: number;
    limit?: number;
}

export class ServiceCenterFilterModel implements ServiceCenterFilter {
    public listId: string[];
    public groupsId: string[];
    public servicesId: string[];
    public name: string;
    public latitude: number;
    public longitude: number;
    public scope: number;
    public isMobile: boolean;
    public skip: number;
    public limit: number;

    constructor(obj: ServiceCenterFilter) {
        _.assign(this, obj);
    }
}

export enum ServiceCenterStatus {
    Active = 1,
    PendingToAccept = 3,
    Deactive = 0,
    PendingToReciveCertificates = 2
}

export interface Schedule {
    weekday?: number;
    startHour?: number;
    endHour?: number;
    startMinute?: number;
    endMinute?: number;
}

export class ScheduleModel implements Schedule {
    public weekday: number;
    public startHour: number;
    public endHour: number;
    public startMinute: number;
    public endMinute: number;

    private _dayOfWeekName = [
        'شنبه',
        'یک شنبه',
        'دو شنبه',
        'سه شنبه',
        'چهار شنبه',
        'پنجشنبه',
        'جمعه'
    ];

    constructor(obj: Schedule) {
        _.assign(this, obj);
    }

    getWeekDayToString() {
        return this._dayOfWeekName[this.weekday - 1];
    }
}

export interface ServiceCenter {
    id?: string;
    ownerId?: string;
    title?: string;
    slogan?: string;
    address?: g.Address;
    category?: string;
    description?: string;
    mobile: string;
    phoneNumbers?: string[];
    background?: string;
    image?: string;
    markerImage?: string;
    location?: [number, number];
    members?: Member[];
    services?: Service[];
    status?: ServiceCenterStatus;
    schedule?: Schedule[];
    attachments?: { id: string; description: string }[];
}

export class ServiceCenterModel implements ServiceCenter {
    public id: string;
    public ownerId: string;
    public title: string;
    public slogan?: string;
    public address: g.Address;
    public categories: string[];
    public background?: string;
    public description: string;
    public mobile: string;
    public phoneNumbers: string[];
    public image: string;
    public markerImage: string;
    public location: [number, number];
    public members: Member[];
    public status: ServiceCenterStatus;
    public services: Service[];
    public schedule: Schedule[];
    public attachments: { id: string; description: string }[];

    constructor(obj?: ServiceCenter) {
        _.assign(this, obj);
    }

    getPhoneNumberStr() {
        return this.phoneNumbers.join(' - ');
    }
}

export interface Member {
    id?: string;
    user?: User;
    job?: string;
    location?: [number, number];
}

export class MemberModel implements Member {
    public id: string;
    public user: User;
    public job: string;
    public location: [number, number];

    constructor(obj: Member) {
        _.assign(this, obj);
    }
}
