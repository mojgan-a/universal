import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../core/auth.guard';
import { AddEditComponent } from './add-edit/add-edit.component';
import { MyServiceCentersComponent } from './my-service-centers/my-service-centers.component';
import { CardViewComponent } from './service-center-list/card-view/card-view.component';
import { MapViewComponent } from './service-center-list/map-view/map-view.component';
import { ServiceCenterComponent } from './service-center.component';
import { SingleComponent } from './single/single.component';

export const routes: Routes = [
    {
        path: '',
        component: ServiceCenterComponent,
        children: [
            {
                path: 'map-view',
                component: MapViewComponent
            },
            {
                path: 'card-view',
                component: CardViewComponent
            },
            {
                path: 'my-service-centers',
                component: MyServiceCentersComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'add',
                component: AddEditComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'edite/:id',
                component: AddEditComponent,
                canActivate: [AuthGuard]
            },
            {
                path: ':id',
                component: SingleComponent
            },
            { path: '', redirectTo: 'map-view', pathMatch: 'full' }
        ]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
