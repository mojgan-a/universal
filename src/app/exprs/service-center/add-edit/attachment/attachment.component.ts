import {
    Component,
    ElementRef,
    Input,
    OnInit,
    Renderer2,
    ViewChild,
    ViewContainerRef,
} from '@angular/core';
import * as _ from 'lodash';
import { ToastsManager } from 'ng6-toastr';
import { ServiceCenterModel } from '../../service-center.model';
import { AddEditService } from '../add-edit.service';

@Component({
    selector: 'app-attachment',
    templateUrl: './attachment.component.html',
    styleUrls: ['./attachment.component.scss']
})
export class AttachmentComponent implements OnInit {
    @Input() sc: ServiceCenterModel;
    @ViewChild('fileInput') fileInput: ElementRef;

    public description: string;
    public loading = false;
    public fileName = '-----';
    public selectFileDirty = false;

    constructor(
        private _addEditService: AddEditService,
        private _renderer2: Renderer2,
        private _tost: ToastsManager,
        // private _vcr: ViewContainerRef
    ) {
        // this._tost.setRootViewContainerRef(_vcr);
    }

    ngOnInit() {}

    public addAttachmentToSC(id) {
        if (_.isNil(this.sc.attachments)) {
            this.sc.attachments = [];
        }

        this.sc.attachments.push({
            id: String(id),
            description: this.description
        });
    }

    public removeAttachmentFromSC(id) {
        if (!_.isNil(this.sc.attachments)) {
            _.remove(this.sc.attachments, fId => {
                return fId === String(id);
            });
        }
    }

    public selectFile() {
        // this.selectFileDirty = true;
        // const event = new MouseEvent('click', { bubbles: true });
        // this._renderer2.invokeElementMethod(
        //     this.fileInput.nativeElement.click()
        //     'dispatchEvent',
        //     [event]

        // );
        this.fileInput.nativeElement.click()
    }

    public uploadFile() {
        if (!this.description) {
            this._tost.error('توضیحات ضروری است.');
            return;
        }

        this.loading = true;
        this._addEditService
            .uploadFile(this.fileInput.nativeElement.files.item(0))
            .subscribe(
                res => {
                    if (res.ok) {
                        this.addAttachmentToSC(res.message);
                        this._tost.success('فایل با موفقیت آپلود شد.');
                    } else {
                        this._tost.error('خطا در سرور!');
                    }
                },
                err => {
                    console.error(err);
                    this.loading = false;
                    this._tost.error('خطا در سیستم!');
                },
                () => {
                    this.loading = false;
                }
            );
    }

    public handleFileInput(files) {
        if (files[0]) {
            if (files[0].size > 5000000) {
                this._tost.error('سایز فایل انتخابی بیشتر از ۵ مگابایت است!');
                return;
            }
            this.fileName = files[0].name;
        } else {
            this.fileName = '-----';
        }
    }

    public removeAttachment(file) {
        const lengthOfServices = this.sc.attachments.length;
        _.remove(this.sc.attachments, at => {
            return at === file;
        });
        if (this.sc.attachments.length === lengthOfServices - 1) {
            this._tost.warning('فایل مورد نظر از لیست حذف شد.');
        }
    }
}
