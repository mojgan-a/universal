import {
    Component,
    ElementRef,
    Input,
    OnChanges,
    OnInit,
    Renderer2,
    ViewChild,
    ViewContainerRef,
} from '@angular/core';
import { latLng, layerGroup, LayerGroup, tileLayer } from 'leaflet';
import * as _ from 'lodash';
import { ToastsManager } from 'ng6-toastr';


import { Observable } from 'rxjs';
import { ServiceCenterModel } from '../../service-center.model';
import { AddEditService } from '../add-edit.service';

@Component({
    selector: 'app-detailes',
    templateUrl: './detailes.component.html',
    styleUrls: ['./detailes.component.scss']
})
export class DetailesComponent implements OnInit, OnChanges {
    @Input() sc: ServiceCenterModel;
    @ViewChild('fileInput') fileInput: ElementRef;

    // Map
    public options = {
        layers: [
            tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                maxZoom: 18,
                attribution: '...'
            })
        ]
    };
    public layers: LayerGroup = layerGroup([]);
    public zoom = 14;
    public center = latLng(35.800397, 51.4091954);

    public categories = [];

    // Picture
    public loading = false;
    public scPic;
    private _scPicOrginal: string;
    private _defualtPic = 'assets/images/image.png';

    constructor(
        private _addEditeService: AddEditService,
        private _renderer: Renderer2,
        private _tost: ToastsManager,
        private _vcr: ViewContainerRef
    ) {
        this._tost.setRootViewContainerRef(_vcr);
        if ('geolocation' in navigator) {
            navigator.geolocation.getCurrentPosition(position => {
                this.center = latLng(
                    position.coords.latitude,
                    position.coords.longitude
                );
            });
        } else {
            this.center = latLng(35.800397, 51.4091954);
        }
    }

    ngOnInit() {
        this._initImage();
        if (_.isNil(this.sc.phoneNumbers) || this.sc.phoneNumbers.length <= 2) {
            this.sc.phoneNumbers = _.fill(Array(1), '');
        }
        this._addEditeService.getCategory().subscribe(res => {
            this.categories = res;
        });
    }

    ngOnChanges() {
        this._initImage();
        console.log(this.sc);
    }

    onMapReady(map) {
        this.layers.addTo(map);
        map.on('moveend', () => {
            // this.mapMoved(map.getCenter());
            const location = map.getCenter();
            this.sc.location = [0, 0];
            this.sc.location[0] = location.lat;
            this.sc.location[1] = location.lng;
        });
    }

    public selectFile() {
        // const event = new MouseEvent('click', { bubbles: true });
        // this._renderer.selectRootElement('#domElementId').scrollIntoView()
        // this._renderer.invokeElementMethod(
        //     this.fileInput.nativeElement,
        //     'dispatchEvent',
        //     [event]
        // );
        this.fileInput.nativeElement.click()
    }

    public uploadPic() {
        this.loading = true;
        this._addEditeService
            .uploadImage(this.fileInput.nativeElement.files.item(0))
            .subscribe(
                res => {
                    if (res.ok) {
                        this.sc.image = res.message;
                        this._tost.success('عکس با موفقیت آپلود شد.');
                    } else {
                        this._tost.error('خطا در سرور!');
                    }
                },
                err => {
                    console.error(err);
                    this.loading = false;
                    this._tost.error('خطا در سیستم!');
                },
                () => {
                    this.loading = false;
                }
            );
    }

    public handleFileInput(files) {
        if (files[0]) {
            if (files[0].size > 5000000) {
                this._tost.error('سایز فایل انتخابی بیشتر از ۵ مگابایت است!');
                return;
            }
            this.uploadPic();
            this._getBase64(files[0]).subscribe(urlBase64 => {
                this.scPic = urlBase64;
            });
        } else {
            this.scPic = this._scPicOrginal;
        }
    }

    private _getBase64(file): Observable<any> {
        return Observable.create(observer => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => {
                observer.next(reader.result);
                observer.complete();
            };
            reader.onerror = error => {
                observer.error(error);
            };
        });
    }

    private _initImage() {
        if (this.sc.image) {
            this._addEditeService.getImagePath(this.sc.image).subscribe(p => {
                this.scPic = p;
                this._scPicOrginal = p;
            });
        } else {
            this.scPic = this._defualtPic;
            this._scPicOrginal = this._defualtPic;
        }
    }
}

