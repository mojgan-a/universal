import {
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnInit,
    Output
} from '@angular/core';

@Component({
    selector: 'app-confirm-price',
    templateUrl: './confirm-price.component.html',
    styleUrls: ['./confirm-price.component.scss']
})
export class ConfirmPriceComponent implements OnInit, OnChanges {
    public plans = [
        {
            duration: '۳ ماهه',
            price: 150000,
        },
        {
            duration: '۶ ماهه',
            price: 270000,
        },
        {
            duration: '۱۲ ماهه',
            price: 420000,
        },
        {
            duration: 'تهیه تیزر تبلیغاتی',
            price: 500000,
        },
        {
            duration: 'تیزر + اشتراک یکساله',
            price: 720000,
        }

    ];

    public tax = 0.09;

    @Input() price: number;
    @Input() show: boolean;
    @Output() submit = new EventEmitter();

    constructor() {}

    ngOnChanges() {
        // if (_.isNil(this.price) || _.isNaN(this.price)) {
        //   this.price = this.basePrice;
        // }
    }

    ngOnInit() {}
}
