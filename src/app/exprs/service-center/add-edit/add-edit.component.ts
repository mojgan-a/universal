import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as _ from 'lodash';
import { Subject } from 'rxjs';
import { Address, ServiceCenterModel } from '../service-center.model';
import { AddEditService } from './add-edit.service';

@Component({
    selector: 'app-add-edit',
    templateUrl: './add-edit.component.html',
    styleUrls: ['./add-edit.component.scss']
})
export class AddEditComponent implements OnInit {
    public formData: ServiceCenterModel = null;
    public changeTab = new Subject();

    constructor(
        private _addEditeService: AddEditService,
        private _router: Router,
        private _route: ActivatedRoute
    ) {}

    ngOnInit() {
        this.initData();

        if (_.isNil(this.formData.address)) {
            this.formData.address = new Address('');
        }
        this._route.params.subscribe(params => {
            if (params.id) {
                this._addEditeService
                    .getServiceCenter(params.id)
                    .subscribe(sc => {
                        this.initData(sc);
                    });
            }
        });
    }

    private initData(data?: ServiceCenterModel) {
        if (_.isNil(data)) {
            this.formData = new ServiceCenterModel();
        } else {
            this.formData = data;
        }
    }
}
