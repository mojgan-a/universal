import { Component, Input, OnInit, ViewContainerRef } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import * as _ from "lodash";
import { ToastsManager } from "ng6-toastr/ng2-toastr";
import { Subject } from "rxjs";
import {
  ServiceCenterModel,
  ValidationResponse
} from "../../service-center.model";
import { AddEditService } from "../add-edit.service";

@Component({
  selector: "app-submit-box",
  templateUrl: "./submit-box.component.html",
  styleUrls: ["./submit-box.component.scss"]
})
export class SubmitBoxComponent implements OnInit {
  @Input() sc: ServiceCenterModel;
  @Input() changeTab: Subject<number>;
  public errors = [];
  public message = null;
  public showModal = false;
  public price = null;

  constructor(
    private _addEditeService: AddEditService,
    private _vcr: ViewContainerRef,
    private _toast: ToastsManager,
    private _router: Router,
    private _route: ActivatedRoute
  ) {
    _toast.setRootViewContainerRef(_vcr);
  }

  ngOnInit() {
    this.message = "";
  }

  priceConfirm() {
    this._router.navigate(["..", "my-service-centers"], {
      relativeTo: this._route
    });
  }

  submit() {
    this.errors = [];
    this.message = "";
    console.log(this.sc);

    if (_.isNil(this.sc.title) || this.sc.title === "") {
      this.errors.push("عنوان در مشخصات ضروری میباشد.");
      return;
    }

    if (_.isNil(this.sc.phoneNumbers) || this.sc.phoneNumbers.length <= 0) {
      this.errors.push("تلفن در مشخصات ضروری میباشد.");
      return;
    }

    // if (_.isNil(this.sc.address.address) || this.sc.address.address === '') {
    //   this.errors.push('آدرس در مشخصات ضروری میباشد.');
    //   return;
    // }

    // if (_.isNil(this.sc.services) || this.sc.services.length <= 0) {
    //   this.errors.push('حداقل انتخاب یک سرویس ضروری است.');
    //   this.changeTab.next(1);
    //   return;
    // }

    if (_.isNil(this.sc.attachments) || this.sc.attachments.length <= 0) {
      this.errors.push("مدرک حقوقی جهت تایید اطلاعات مورد نیاز است.");
      this.changeTab.next(3);
      return;
    }

    this._addEditeService.addServiceCenter(this.sc).subscribe(
      (res: ValidationResponse) => {
        console.log("[submit-box.component] this.sc.id:", this.sc.id);
        if (res.ok) {
          this.message = "مرکز خدماتی در رگلاژ ثبت شد.";
          let showModal = false;
          if (this.sc.id === undefined) {
            showModal = true;
          } 
          this.price = res.data.price;
          setTimeout(() => {
            this.showModal = showModal;
          }, 1000);
        } else {
          this.errors.push("خطا از سرور!");
        }
      },
      err => {
        this.errors.push("خطا از سامانه!");
      }
    );
  }
}
