import { Component, Input, OnInit, ViewContainerRef } from '@angular/core';
import * as _ from 'lodash';
import { ToastsManager } from 'ng6-toastr/ng2-toastr';
import { ServiceCenterModel } from '../../service-center.model';
import { AddEditService } from '../add-edit.service';

@Component({
    selector: 'app-services',
    templateUrl: './services.component.html',
    styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {
    @Input() sc: ServiceCenterModel;

    public serviceSearchResult: any[] = [];
    public carModelResult: any[] = [];
    public SelectedService = null;
    public selectedServiceCarModel = null;
    public price;

    constructor(
        private _addEditService: AddEditService,
        private _tost: ToastsManager,
        private _vcr: ViewContainerRef
    ) {
        this._tost.setRootViewContainerRef(_vcr);
    }

    ngOnInit() {
        this.getLists();
    }

    onChangeService(s) {
        this.SelectedService = s;
    }

    onChangeServiceCarModel(m) {
        this.selectedServiceCarModel = m;
    }

    // getSearchResult(term) {
    //   this._addEditService.getServiceList(term).subscribe((res) => {
    //     this.serviceSearchResult = res;
    //   }, err => {
    //     alert(err);
    //   });
    // }

    getLists() {
        this._addEditService.getServiceList().subscribe(
            res => {
                this.serviceSearchResult = res;
            },
            err => {
                alert('errore!');
                console.error(err);
            }
        );
    }

    getCarModel(term) {
        this._addEditService.searchModelBrand(term).subscribe(
            res => {
                this.carModelResult = res;
            },
            err => {
                alert('errore!');
                console.error(err);
            }
        );
    }

    addService() {
        if (_.isNil(this.SelectedService)) {
            this._tost.error('سرویسی انتخاب نشده!');
            return;
        }

        if (_.isNil(this.price)) {
            this._tost.error('قیمت مشخص نشده!');
            return;
        }

        if (_.isNil(this.selectedServiceCarModel)) {
            this._tost.error('مدل خودرو مشخص نشده!');
            return;
        }

        const service = this.SelectedService;
        service.price = this.price;
        service.company = this.selectedServiceCarModel;
        if (_.isNil(this.sc.services)) {
            this.sc.services = [];
        }
        this.sc.services.push(service);
        this._tost.success('سرویس به مجموعه اضافه شد.');
    }

    removeService(service) {
        const lengthOfServices = this.sc.services.length;
        _.remove(this.sc.services, s => {
            return s === service;
        });
        if (this.sc.services.length === lengthOfServices - 1) {
            this._tost.warning('سرویس مورد نظر از لیست حذف شد.');
        }
    }
}
