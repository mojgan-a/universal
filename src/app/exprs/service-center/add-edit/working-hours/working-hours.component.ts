import { Component, Input, OnChanges, OnInit } from '@angular/core';
import * as moment from 'jalali-moment';
import * as _ from 'lodash';
import { ServiceCenterModel } from '../../service-center.model';

@Component({
    selector: 'app-working-hours',
    templateUrl: './working-hours.component.html',
    styleUrls: ['./working-hours.component.scss']
})
export class WorkingHoursComponent implements OnInit, OnChanges {
    @Input() sc: ServiceCenterModel;

    schedule = [
        {
            title: 'شنبه ',
            weekday: 1,
            isActive: false,
            startTime: moment('08:00', 'HH:mm'),
            endTime: moment('10:00', 'HH:mm')
        },
        {
            title: 'یکشنبه ',
            isActive: false,
            weekday: 2,
            startTime: moment('08:00', 'HH:mm'),
            endTime: moment('10:00', 'HH:mm')
        },
        {
            title: 'دوشنبه ',
            weekday: 3,
            isActive: false,
            startTime: moment('08:00', 'HH:mm'),
            endTime: moment('10:00', 'HH:mm')
        },
        {
            title: 'سه شنبه ',
            weekday: 4,
            isActive: false,
            startTime: moment('08:00', 'HH:mm'),
            endTime: moment('10:00', 'HH:mm')
        },
        {
            title: 'چهارشنبه ',
            weekday: 5,
            isActive: false,
            startTime: moment('08:00', 'HH:mm'),
            endTime: moment('10:00', 'HH:mm')
        },
        {
            title: 'پنجشنبه ',
            weekday: 6,
            isActive: false,
            startTime: moment('08:00', 'HH:mm'),
            endTime: moment('10:00', 'HH:mm')
        },
        {
            title: 'جمعه ',
            weekday: 7,
            isActive: false,
            startTime: moment('08:00', 'HH:mm'),
            endTime: moment('10:00', 'HH:mm')
        }
    ];

    constructor() {}

    ngOnInit() {}

    ngOnChanges() {
        if (!_.isNil(this.sc.schedule) && _.isArray(this.sc.schedule)) {
            this.sc.schedule.forEach(s => {
                this.schedule = this.schedule.map(sm => {
                    if (sm.weekday === _.toNumber(s.weekday)) {
                        return {
                            title: sm.title,
                            weekday: sm.weekday,
                            isActive: true,
                            startTime: moment()
                                .set('hour', s.startHour)
                                .set('minute', s.startMinute),
                            endTime: moment()
                                .set('hour', s.endHour)
                                .set('minute', s.endMinute)
                        };
                    }
                    return sm;
                });
            });
        }
    }

    updateSchedule() {
        const result = this.schedule
            .filter(s => {
                return s.isActive;
            })
            .map(s => {
                return {
                    weekday: s.weekday,
                    startHour: s.startTime.hour(),
                    endHour: s.endTime.hour(),
                    startMinute: s.startTime.minute(),
                    endMinute: s.endTime.minute()
                };
            });
        this.sc.schedule = result;
    }
}
