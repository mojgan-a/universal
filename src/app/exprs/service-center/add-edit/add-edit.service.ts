
import {tap, map, catchError} from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as _ from 'lodash';







import { Observable } from 'rxjs';
import { BaseService } from '../../../core/base-service';
import { UserModel } from '../../user/user.model';
import { ModelBrandModel } from '../../vehicle/vehicle.model';
import {
    Address,
    MemberModel,
    ScheduleModel,
    ServiceCenterModel,
    ServiceCenterStatus,
    ServiceGroupModel,
    ServiceModel,
    ValidationResponse
} from '../service-center.model';

const catList = [
    'CNG تجهیزات',
    'اسپرتی',
    'اگزوز سازی',
    'بوستر سازی',
    'پارکینگ',
    'پالایش و پخش فیلتر و روغن',
    'پمپ سازی',
    'تراشکاری',
    'تعمیرات جعبه فرمان و پمپ هیدرولیک',
    'تون آپ',
    'جلوبندی',
    'رادیاتور سازی',
    'رینگ و لاستیک فروشی',
    'سیم پیچی و باتری سازی',
    'شیشه و آیینه اتومبیل',
    'صافکاری و نقاشی',
    'فروشگاه باتری',
    'کارواش',
    'مکانیکی',
    'لوازم یدکی'
];

@Injectable()
export class AddEditService extends BaseService {
    serviceCenterRepo: ServiceCenterModel;
    serviceCenterRepoUpdated = new EventEmitter();
    carBrandsModelsRepo: ModelBrandModel[] = null;

    constructor(
        private _http: HttpClient,
        private _route: ActivatedRoute,
        private _router: Router
    ) {
        super();
    }

    getCategory(): Observable<any> {
        return this._http.get(this._apiUrl.getServiceGroup).pipe(map((res: any) => {
            const result = [];
            res.categories.forEach(c => {
                result.push(
                    new ServiceGroupModel({
                        id: c,
                        title: c
                    })
                );
            });
            return result;
        }));
    }

    addServiceCenter(sc: ServiceCenterModel): Observable<ValidationResponse> {
        // const services = [];
        // if (_.isArray(sc.services)) {
        //     sc.services.forEach((s: ServiceModel) => {
        //         if (
        //             !_.isNil(s.company) &&
        //             !_.isNil(s.company.id) &&
        //             !_.isNil(s.id)
        //         ) {
        //             services.push({
        //                 referenceId: _.toNumber(s.referenceId),
        //                 price: _.toNumber(s.price),
        //                 modelId: _.toNumber(s.company.id)
        //             });
                    
        //         }
               
        //     });
            
        // }
        
        const services = [];
        if (_.isArray(sc.services)) {
            sc.services.forEach((s: ServiceModel) => {
                if (
                    !_.isNil(s.company) &&
                    !_.isNil(s.company.id) &&
                    !_.isNil(s.id)
                ) {
                  if (s.id.length === 24) {
                    services.push({
                        referenceId: _.toNumber(s.referenceId),
                        price: _.toNumber(s.price),
                        modelId: _.toNumber(s.company.id)
                    });
                  } else {
                    services.push({
                      referenceId: _.toNumber(s.id),
                      price: _.toNumber(s.price),
                      modelId: _.toNumber(s.company.id)
                  });
                  }
                }
            });
        }
        

        const members = [];
        if (_.isArray(sc.members)) {
            sc.members.forEach((m: MemberModel) => {
                members.push({
                    id: _.toString(m.user.id),
                    job: _.toString(m.job)
                });
            });
        }

        sc.phoneNumbers = sc.phoneNumbers.map(p => {
            return _.toString(p);
        });

        let attachments = null;
        if (sc.attachments) {
            attachments = sc.attachments.map(a => {
                return {
                    description: a.description,
                    fileId: a.id
                };
            });
        }

        const data = {
            name: sc.title,
            imageId: sc.image,
            description: sc.description,
            address: sc.address.address,
            latitude: sc.location[0],
            longitude: sc.location[1],
            isMobile: false,
            mobile: _.toString(sc.mobile),
            advertisement: sc.slogan,
            background: sc.background,
            categories: sc.categories,
            phoneNumbers: sc.phoneNumbers,
            services: services,
            members: members,
            schedule: sc.schedule,
            attachments: attachments
        };
        console.log('dataaaaaaaaa:'+data.services);
        const mapResponse = (res: any) => {
            return new ValidationResponse(res.message, true, {
                price: 50000
            });
        };

        if (_.isNil(sc.id)) {
            return this._http
                .post(this._apiUrl.serviceCenterC, data).pipe(
                map(mapResponse),
                catchError(this._catchBadResponse),);
        } else {
            console.log('s.id:'+data);
            return this._http
                .put(this._apiUrl.serviceCenterRUD({ id: sc.id }), data).pipe(
                
                map(mapResponse),
                catchError(this._catchBadResponse),);
        }
    }

    getUserByPhoneNumber(phoneNumber) {
        return this._http
            .post(this._apiUrl.checkUserName, {
                username: phoneNumber
            }).pipe(
            map((res: any) => {
                return new UserModel({
                    id: res.userId,
                    phoneNumber: phoneNumber,
                    firstName: res.firstName,
                    lastName: res.lastName
                });
            }));
    }

    // getServiceList(term: string): Observable<ServiceModel[]> {
    //   let params = new HttpParams().set('search', term);
    //   return this._http.get(this._apiUrl.getServices, {params: params}).map((res: any) => {
    //     let result = [];
    //     if (_.isNil(res.services)) {
    //       return result;
    //     }

    //     res.services.forEach(service => {
    //       result.push(new ServiceModel({
    //         id: service.referenceId,
    //         title: service.name,
    //         description: service.description,
    //         price: null,
    //         isMobile: null,
    //         category: service.category,
    //       }))
    //     });

    //     return result
    //   })
    // }

    getServiceList() {
        return this._http.get(this._apiUrl.getServices).pipe(map((res: any) => {
            const result = [];
            if (_.isNil(res.services)) {
                return result;
            }

            res.services.forEach(service => {
                result.push(
                    new ServiceModel({
                        id: service.id,
                        title: service.name,
                        description: service.description,
                        price: null,
                        isMobile: null,
                        category: service.category,
                        company: null
                    })
                );
            });

            return result;
        }));
    }

    getCarModelList() {
        return this._http.get(this._apiUrl.vmsBrand).pipe(map((res: any) => {
            const result = [];
            res.brands.forEach(b => {
                b.models.forEach(model => {
                    result.push(
                        new ModelBrandModel({
                            id: model.id,
                            title: model.name,
                            company: {
                                icon: null,
                                id: b.id,
                                title: b.slug,
                                image: b.imageId
                            }
                        })
                    );
                });
            });
            return result;
        }));
    }

    searchModelBrand(term) {
        return Observable.create(observer => {
            if (_.isNil(this.carBrandsModelsRepo)) {
                this.getCarModelList().subscribe(res => {
                    this.carBrandsModelsRepo = res;
                    console.log(this._getFilteredCarBrand(term));
                    observer.next(this._getFilteredCarBrand(term));
                });
            } else {
                console.log(this._getFilteredCarBrand(term));
                observer.next(this._getFilteredCarBrand(term));
            }
        });
    }

    private _getFilteredCarBrand(term) {
        return this.carBrandsModelsRepo.filter(bm => {
            const cr = bm.title.search(term);
            const mr = bm.company.title.search(term);
            return cr !== -1 || mr !== -1;
        });
    }

    getImagePath(id) {
        return Observable.create(observer => {
            let path = this._apiUrl.getImage({ id: id });
            path = this._baseUrl + path.slice(3);
            observer.next(path);
            observer.complete();
        });
    }

    // uploadAttachmentFile(fileToUpload: File): Observable<ValidationResponse> {
    //   const formData: FormData = new FormData();
    //   formData.append('file', fileToUpload, fileToUpload.name);
    //   return this._http.post(this._apiUrl.uploadFile, formData).map((res: any) => {
    //     return new ValidationResponse(res.fileId);
    //   });
    // }

    uploadFile(fileToUpload: File): Observable<ValidationResponse> {
        const formData: FormData = new FormData();
        formData.append('file', fileToUpload, fileToUpload.name);
        return this._http
            .post(this._apiUrl.uploadFile, formData).pipe(
            map((res: any) => {
                return new ValidationResponse(res.fileId);
            }));
    }

    uploadImage(fileToUpload: File): Observable<ValidationResponse> {
        const formData: FormData = new FormData();
        formData.append('file', fileToUpload, fileToUpload.name);
        return this._http
            .post(this._apiUrl.uploadServiceCenterImage, formData).pipe(
            map((res: any) => {
                return new ValidationResponse(res.fileId);
            }));
    }

    getServiceCenter(id: string) {
        return this._http
            .get(
                this._apiUrl.serviceCenterRUD({
                    id: id
                })
            ).pipe(
            map((res: any) => {
                let attachments = [];
                if (!_.isNil(res.attachments) && _.isArray(res.attachments)) {
                    attachments = res.attachments.pipe(map( ({a}) => {
                        return {
                            id: a.fileId,
                            description: a.description
                        };
                    }));
                }

                let members = [];
                if (!_.isNil(res.members) && _.isArray(res.members)) {
                    members = res.members.pipe(map( ({m}) => {
                        return new MemberModel({
                            id: m.id,
                            job: m.job,
                            location: null,
                            user: new UserModel({
                                id: m.userId,
                                firstName: m.firstName,
                                lastName: m.lastName,
                                phoneNumber: m.phoneNumber
                            })
                        });
                    }));
                }

                let phoneNumbers = [];
                if (!_.isNil(res.phoneNumbers) && _.isArray(res.phoneNumbers)) {
                    phoneNumbers = res.phoneNumbers.pipe(map(p => {
                        return p;
                    }));
                }

                let schedule = [];
                if (!_.isNil(res.schedule) && _.isArray(res.schedule)) {
                    schedule = res.schedule.pipe(map( ({s}) => {
                        return new ScheduleModel({
                            weekday: s.weekday,
                            startHour: s.startHour,
                            endHour: s.endHour,
                            endMinute: s.endMinute,
                            startMinute: s.startMinute
                        });
                    }));
                }

                let services = [];
                if (!_.isNil(res.services) && _.isArray(res.services)) {
                    services = res.services.pipe(map( ({s}) => {
                        if (_.isNil(s.model) || _.isNil(s.brand)) {
                            return new ServiceModel({});
                        }
                        return new ServiceModel({
                            id: s.id,
                            title: s.name,
                            category: new ServiceGroupModel({
                                id: s.category,
                                title: s.category
                            }),
                            company: new ModelBrandModel({
                                id: s.model.id,
                                title: s.model.name,
                                company: {
                                    id: s.brand.id,
                                    title: s.brand.name,
                                    image: s.brand.imageId,
                                    icon: null
                                }
                            }),
                            price: s.price,
                            referenceId:s.referenceId,
                            
                        });
                    }));
                }

                const result = new ServiceCenterModel({
                    id: _.isNil(res.id) ? null : res.id,
                    address: _.isNil(res.address)
                        ? null
                        : new Address(res.address),
                    attachments: attachments,
                    background: _.isNil(res.background) ? null : res.background,
                    category:
                        _.isNil(res.categories) && !_.isArray(res.categories)
                            ? null
                            : res.categories,
                    description: _.isNil(res.description)
                        ? null
                        : res.description,
                    image: _.isNil(res.imageId) ? null : res.imageId,
                    location: [res.latitude, res.longitude],
                    markerImage: null,
                    members: members,
                    mobile: _.isNil(res.mobile) ? null : res.mobile,
                    ownerId: _.isNil(res.ownerId) ? null : res.ownerId,
                    phoneNumbers: phoneNumbers,
                    title: _.isNil(res.name) ? null : res.name,
                    slogan: _.isNil(res.advertisement)
                        ? null
                        : res.advertisement,
                    schedule: schedule,
                    services: services,
                    status: res.isConfirmed
                        ? ServiceCenterStatus.Active
                        : ServiceCenterStatus.Deactive
                });
                return result;
            }),
            tap(res => {
                this.serviceCenterRepo = res;
                this.serviceCenterRepoUpdated.emit(res);
            }),);
    }

    private _getCatImage(cats) {
        let cat;
        if (cats && cats[0] && catList.indexOf(cats[0]) > -1) {
            cat = cats[0];
        } else {
            cat = 'default';
        }
        return `assets/images/service-center-images/cat-${cat}.svg`;
    }

    private _getCatText(cat) {
        return 'مکانیکی';
    }
}
