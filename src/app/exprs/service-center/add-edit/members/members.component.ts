import { Component, Input, OnInit, ViewContainerRef } from '@angular/core';
import * as _ from 'lodash';
import { ToastsManager } from 'ng6-toastr/ng2-toastr';
import { UserModel } from '../../../user/user.model';
import { MemberModel, ServiceCenterModel } from '../../service-center.model';
import { AddEditService } from '../add-edit.service';

@Component({
    selector: 'app-members',
    templateUrl: './members.component.html',
    styleUrls: ['./members.component.scss']
})
export class MembersComponent implements OnInit {
    @Input() sc: ServiceCenterModel;

    public users: UserModel[];
    public selectedMember: UserModel;
    public job: string;

    constructor(
        private _addEditService: AddEditService,
        private _tost: ToastsManager,
        private _vcr: ViewContainerRef
    ) {
        this._tost.setRootViewContainerRef(_vcr);
    }

    ngOnInit() {}

    getSearchResult(term) {
        this._addEditService.getUserByPhoneNumber(term).subscribe(res => {
            const option = res;
            option['title'] = `${option.firstName} ${option.lastName}`;
            this.users = [option];
            console.log(this.users);
        });
    }

    onChangeMember(member) {
        this.selectedMember = member;
    }

    removeMember(member) {
        const lengthOfServices = this.sc.members.length;
        _.remove(this.sc.members, s => {
            return s === member;
        });
        if (this.sc.members.length === lengthOfServices - 1) {
            this._tost.warning('عضو مورد نظر پاک حذف شده.');
        }
    }

    addMember() {
        if (_.isNil(this.selectedMember)) {
            this._tost.error('عضو انتخاب نشده!');
            return;
        }

        if (_.isNil(this.job)) {
            this._tost.error('مسولیت عضو مشخص نشده!');
            return;
        }

        const member = new MemberModel({
            user: this.selectedMember,
            job: this.job
        });
        if (_.isNil(this.sc.members)) {
            this.sc.members = [];
        }
        this.sc.members.push(member);
        this._tost.success('سرویس به مجموعه اضافه شد.');
    }
}
