
import {tap, map, catchError} from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as _ from 'lodash';







import { environment } from '../../../../environments/environment';
import { AuthService } from '../../../core/auth.service';
import { BaseService } from '../../../core/base-service';
import {
    ServiceCenterModel,
    ValidationResponse
} from '../service-center.model';
import * as AES256 from "aes-everywhere";
const catList = [
    'CNG تجهیزات',
    'اسپرتی',
    'اگزوز سازی',
    'بوستر سازی',
    'پارکینگ',
    'پالایش و پخش فیلتر و روغن',
    'پمپ سازی',
    'تراشکاری',
    'تعمیرات جعبه فرمان و پمپ هیدرولیک',
    'تون آپ',
    'جلوبندی',
    'رادیاتور سازی',
    'رینگ و لاستیک فروشی',
    'سیم پیچی و باتری سازی',
    'شیشه و آیینه اتومبیل',
    'صافکاری و نقاشی',
    'فروشگاه باتری',
    'کارواش',
    'مکانیکی',
    'لوازم یدکی'
];

@Injectable()
export class MyServiceCentersService extends BaseService {
    serviceCenterRepo: ServiceCenterModel[];
    serviceCenterRepoUpdated = new EventEmitter();

    constructor(
        private http: HttpClient,
        private route: ActivatedRoute,
        private router: Router,
        private auth: AuthService
    ) {
        super();
    }

    deleteServiceCenter(id: string) {
        return this.http
            .delete(
                this._apiUrl.serviceCenterRUD({
                    id: id
                })
            ).pipe(
            map((res: any) => {
                _.remove(this.serviceCenterRepo, i => {
                    return _.toString(i.id) === id;
                });
                return new ValidationResponse(res.message);
            }),
            catchError(this._catchBadResponse),);
    }

    getMyserviceCenter() {
        return this.http
            .post(this._apiUrl.getServiceCenters, {
                ownerId: this.auth.getUser().id
            }).pipe(
            map((res: any) => {
                let tmp = AES256.decrypt(res.providers, "179425993");
                tmp = JSON.parse(tmp);
                const result = [];
                
                if (_.isNil(tmp)) {
                    return result;
                }
                tmp.forEach(sc => {
                    const services = [];
                    let image;
                    if (sc.imageId) {
                        image =
                            environment.baseUrl +
                            this._apiUrl.getImage({ id: sc.imageId }).slice(3);
                    } else {
                        image = this._getCatImage(sc.categories);
                    }

                    let desc;
                    if (sc.categories && _.isArray(sc.categories)) {
                        desc = sc.categories.join('-');
                    } else {
                        desc = '-';
                    }

                    result.push(
                        new ServiceCenterModel({
                            id: sc.id,
                            title: sc.name,
                            address: sc.address,
                            category: null,
                            image: image,
                            markerImage: null,
                            description: desc,
                            mobile: '',
                            phoneNumbers: [''],
                            location: [sc.latitude, sc.longitude],
                            members: null,
                            services: services
                        })
                    );
                });
                return result;
            }),
            tap(res => {
                this.serviceCenterRepo = res;
                this.serviceCenterRepoUpdated.emit(res);
            }),);
    }

    private _getCatImage(cats) {
        let cat;
        if (cats && cats[0] && catList.indexOf(cats[0]) > -1) {
            cat = cats[0];
        } else {
            cat = 'default';
        }
        return `assets/images/service-center-images/cat-${cat}.svg`;
    }

    private _getCatText(cat) {
        return 'مکانیکی';
    }
}
