import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng6-toastr/ng2-toastr';
import { ConfirmationDialogService } from '../../../components/confirmation-dialog/confirmation-dialog.service';
import { MyServiceCentersService } from './my-service-centers.service';

@Component({
    selector: 'app-my-service-centers',
    templateUrl: './my-service-centers.component.html',
    styleUrls: ['./my-service-centers.component.scss']
})
export class MyServiceCentersComponent implements OnInit {
    // public serviceCenters: ServiceCenter[] = [];

    constructor(
        private _scs: MyServiceCentersService,
        private _cs: ConfirmationDialogService,
        private _tost: ToastsManager,
        private _vcr: ViewContainerRef
    ) {
        this._tost.setRootViewContainerRef(_vcr);
    }

    ngOnInit() {
        this._scs.getMyserviceCenter().subscribe();
    }

    handleDeleteClick(id) {
        this._cs
            .confirm('آیا اطمینان از حذف این سرویس سنتر دارید؟')
            .subscribe(status => {
                this._scs.deleteServiceCenter(id).subscribe(
                    res => {
                        if (res.ok) {
                            this._tost.success(
                                'مرکز خدماتی مورد نظر از سیستم حذف شد.'
                            );
                        } else {
                            this._tost.error('خطا در سرور!');
                        }
                    },
                    err => {
                        console.error(err);
                        this._tost.error('خطا در سیستم!');
                    }
                );
            });
    }

    public get serviceCenters() {
        return this._scs.serviceCenterRepo;
    }
}
