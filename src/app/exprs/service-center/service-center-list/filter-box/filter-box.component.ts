import {
    animate,
    state,
    style,
    transition,
    trigger
} from '@angular/animations';
import { Component, Input, OnInit } from '@angular/core';
import { ServiceGroup } from '../../service-center.model';
import { ServiceCenterListService } from '../service-center-list.service';

interface FilterForm {
    groups: ServiceGroup[];
    name: string;
    isMobile: '' | '0' | '1';
}

@Component({
    selector: 'app-filter-box',
    templateUrl: './filter-box.component.html',
    styleUrls: ['./filter-box.component.scss'],
    animations: [
        trigger('visibilityChanged', [
            state('shown', style({ opacity: 1, height: '200px' })),
            state('hidden', style({ opacity: 0, height: '0' })),
            transition('hidden <=> shown', animate('600ms ease-out'))
        ])
    ]
})
export class FilterBoxComponent implements OnInit {
    visibility = 'shown';

    public filterForm: FilterForm = {
        groups: null,
        name: null,
        isMobile: ''
    };
    public inVisible = false;

    @Input() isVisible = true;

    ngOnChanges() {
        this.visibility = this.isVisible ? 'shown' : 'hidden';
    }

    toggleVisibility() {
        if (this.visibility === 'shown') {
            this.visibility = 'hidden';
        } else {
            this.visibility = 'shown';
        }
    }

    constructor(private _serviceCenterListService: ServiceCenterListService) {}

    ngOnInit() {
        this._serviceCenterListService.getServiceGroup().subscribe();
    }

    onSubmit() {
        let ism = null;
        let groups = null;

        if (this.filterForm.isMobile === '0') {
            ism = false;
        } else if (this.filterForm.isMobile === '1') {
            ism = true;
        }

        if (this.filterForm.groups && this.filterForm.groups.length !== 0) {
            groups = this.filterForm.groups;
        }

        this._serviceCenterListService.setFilter(
            groups,
            this.filterForm.name,
            ism
        );
    }

    get serviceGroups() {
        return this._serviceCenterListService.serviceGroupsRepo;
    }
}
