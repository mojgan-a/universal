
import {tap, map} from 'rxjs/operators';
import { HttpClient } from "@angular/common/http";
import { EventEmitter, Injectable } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import * as _ from "lodash";
import "rxjs/add/observable/of";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/debounceTime";
import "rxjs/add/operator/delay";
import "rxjs/add/operator/do";
import "rxjs/add/operator/finally";
import "rxjs/add/operator/map";
import * as AES256 from "aes-everywhere";
import { Observable } from "rxjs";
import { BaseService } from "../../../core/base-service";
import {
  ServiceCenter,
  ServiceCenterFilter,
  ServiceCenterModel,
  ServiceGroup,
  ServiceGroupModel
} from "../service-center.model";

const catList = [
  "CNG تجهیزات",
  "اسپرتی",
  "اگزوز سازی",
  "بوستر سازی",
  "پارکینگ",
  "پالایش و پخش فیلتر و روغن",
  "پمپ سازی",
  "تراشکاری",
  "تعمیرات جعبه فرمان و پمپ هیدرولیک",
  "تون آپ",
  "جلوبندی",
  "رادیاتور سازی",
  "رینگ و لاستیک فروشی",
  "سیم پیچی و باتری سازی",
  "شیشه و آیینه اتومبیل",
  "صافکاری و نقاشی",
  "فروشگاه باتری",
  "کارواش",
  "مکانیکی",
  "لوازم یدکی"
];

@Injectable()
export class ServiceCenterListService extends BaseService {
  serviceCenterRepo: ServiceCenter[] = [];
  serviceCenterRepoUpdated = new EventEmitter();
  serviceGroupsRepo: ServiceGroup[] = [];
  serviceGroupsRepoUpdated = new EventEmitter();
  serviceCenterFilter: ServiceCenterFilter = {};
  serviceCenterFilterUpdated = new EventEmitter();

  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router
  ) {
    super();
    this.serviceCenterFilterUpdated.subscribe(() => {
      this.getServiceCenters().subscribe();
    });
  }

  setFilter(groups?: ServiceGroup, title?: string, isMobile?: boolean) {
    let groupsId = null;
    if (groups) {
      groupsId = groups.id;
    }
    this.serviceCenterFilter.category = groupsId;
    this.serviceCenterFilter.name = title;
    if (_.isBoolean(isMobile)) {
      this.serviceCenterFilter.isMobile = isMobile;
    } else {
      this.serviceCenterFilter.isMobile = null;
    }
    // this.getServiceCenters().subscribe();
    this.serviceCenterFilterUpdated.emit(this.serviceCenterFilter);
  }

  getServiceGroup(): Observable<ServiceGroup[]> {
    return this.http
      .get(this._apiUrl.getServiceGroup).pipe(
      map((res: any) => {
        const result = [];
        if (_.isNil(res.categories)) {
          return result;
        }
        res.categories.forEach(g => {
          result.push(
            new ServiceGroupModel({
              id: g,
              title: g
            })
          );
        });
        return result;
      }),
      tap(res => {
        this.serviceGroupsRepo = res;
        this.serviceGroupsRepoUpdated.emit(res);
      }),);
  }

  getServiceCenters(): Observable<ServiceCenter[]> {
    return this.http
      .post(this._apiUrl.getServiceCenters, this.serviceCenterFilter).pipe(
      map((res: any) => {
        let tmp = AES256.decrypt(res.providers, "179425993");
       
        tmp = JSON.parse(tmp);
        const result = [];
        if (_.isNil(res.providers)) {
          return result;
        }
        tmp.forEach(sc => {
          const services = [];
          let image;
          if (sc.image) {
            image = sc.image;
          } else {
            image = this._getCatImage(sc.categories);
          }

          let desc;
          if (sc.categories && _.isArray(sc.categories)) {
            desc = sc.categories.join("-");
          } else {
            desc = "-";
          }

          result.push(
            new ServiceCenterModel({
              id: sc.id,
              title: sc.name,
              address: sc.address,
              category: this._getCatText("mechanic"),
              image: image,
              markerImage: this._getCatMarkerImage(sc.categories),
              description: desc,
              mobile: "09xxxxxxxxx",
              phoneNumbers: ["021xxxxxxxx"],
              location: [sc.latitude, sc.longitude],
              members: null,
              services: services
            })
          );
        });
        return result;
      }),
      tap(res => {
        this.serviceCenterRepo = res;
        this.serviceCenterRepoUpdated.emit(res);
      }),);
  }

  private _getCatMarkerImage(cats): string {
    let cat;
    if (cats && cats[0] && catList.indexOf(cats[0]) > -1) {
      cat = cats[0];
    } else {
      cat = "default";
    }
    return `assets/images/markers/cat-${cat}.svg`;
  }

  private _getCatImage(cats) {
    let cat;
    if (cats && cats[0] && catList.indexOf(cats[0]) > -1) {
      cat = cats[0];
    } else {
      cat = "default";
    }
    return `assets/images/service-center-images/cat-${cat}.svg`;
  }

  private _getCatText(cat) {
    return "مکانیکی";
  }
}
