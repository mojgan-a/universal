import { Component, OnInit } from '@angular/core';
import { ServiceCenter } from '../../service-center.model';
import { ServiceCenterListService } from '../service-center-list.service';

@Component({
    selector: 'app-card-view',
    templateUrl: './card-view.component.html',
    styleUrls: ['./card-view.component.scss']
})
export class CardViewComponent implements OnInit {
    public showModal = false;
    private _scope = 1;
    private _lengthOfElement = 10;
    public canGoDown = true;
    public serviceCenters: ServiceCenter[] = [];
    public  hidePopup:any = {};

    constructor(private _serviceCenterListService: ServiceCenterListService) {}

    ngOnInit() {
        this._init();
        this._serviceCenterListService.serviceCenterFilterUpdated.subscribe(
            () => {
                this._init();
            }
        );
    }

    onScroll() {
        if (!this.canGoDown) {
            return;
        }
        this._scope++;
        this._serviceCenterListService.serviceCenterFilter.skip =
            this._lengthOfElement * (this._scope - 1);
        this._serviceCenterListService.getServiceCenters().subscribe(
            scs => {
                this.serviceCenters = this.serviceCenters.concat(scs);
            },
            err => {
                this.canGoDown = false;
            }
        );
    }
    //public showModal = false;
    onReserveClick(event) {
        console.log('[CardViewComponent.onReserveClick] event:', event);
        //this.showModal = true;
    }

    private _init() {
        this._serviceCenterListService.serviceCenterFilter.skip = 0;
        this._serviceCenterListService.serviceCenterFilter.limit = 10;
        this._serviceCenterListService
            .getServiceCenters()
            .subscribe(scs => (this.serviceCenters = scs));
    }
}
