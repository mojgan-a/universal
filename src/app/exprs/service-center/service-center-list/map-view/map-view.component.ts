import { Component, OnInit } from "@angular/core";
import {
  icon,
  latLng,
  layerGroup,
  LayerGroup,
  marker,
  point,
  tileLayer
} from "leaflet";
import { ServiceCenter } from "../../service-center.model";
import { ServiceCenterListService } from "../service-center-list.service";
import { Router } from '@angular/router';

@Component({
  selector: "app-map-view",
  templateUrl: "./map-view.component.html",
  styleUrls: ["./map-view.component.scss"]
})
export class MapViewComponent implements OnInit {
  public options = {
    layers: [
      tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
        maxZoom: 18,
        attribution: "..."
      })
    ]
  };
  public layers: LayerGroup = layerGroup([]);
  public zoom = 14;
  public allowChange = false;

  public center = latLng(35.800397, 51.4091954);

  constructor(private _serviceCenterListService: ServiceCenterListService, private router : Router) {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(position => {
        this.center = latLng(
          position.coords.latitude,
          position.coords.longitude
        );
      });
    } else {
      this.center = latLng(35.800397, 51.4091954);
    }
  }

  ngOnInit() {

    // this.href = this.router.url;
    // const currentUrl = JSON.parse(localStorage.getItem(url));
    // const url = this.router.url;
    // const coordinates = this.router.parseUrl(url).queryParams['l'];

    const coordinates = sessionStorage.getItem('coordinates');
    console.log('[map-view.component.ngOnInit] previousLocation:', coordinates);

    if (coordinates !== null) {
      const commaIndex = coordinates.indexOf(',');
      const latitude = parseFloat(coordinates.substring(0, commaIndex));
      const longitude = parseFloat(coordinates.substring(commaIndex + 1, coordinates.length));
      const previousLocation = latLng(latitude, longitude);
      console.log('location:', coordinates, "previousLocation:", previousLocation);
      this.center = previousLocation;
    } else {
      console.log('[map-view.component.ngOnInit] coordinates not available');
      // const currentUrl = localStorage.getItem('coordinates');
      // const currentUrl = JSON.parse(localStorage.getItem(url));
    }

    this._serviceCenterListService.serviceCenterFilter.skip = 0;
    this._serviceCenterListService.serviceCenterFilter.limit = 0;
    this._serviceCenterListService.serviceCenterFilter.scope = 2000;
    this._serviceCenterListService.serviceCenterFilter.longitude = this.center.lng;
    this._serviceCenterListService.serviceCenterFilter.latitude = this.center.lat;
    
    this._serviceCenterListService.getServiceCenters().subscribe(scs => {
      this.resetMarkers();
      this.renderMarkerOnMap(scs);
    });

    this._serviceCenterListService.serviceCenterRepoUpdated.subscribe(scs => {
      this.resetMarkers();
      this.renderMarkerOnMap(scs);
      //console.log(scs);
      this.allowChange = false;
      // if (scs.length > 0) {
      //   this.center = scs[0].location;
      //   console.log(this.center);
      // }
      //this.allowChange=true;
    });
  }

  renderMarkerOnMap(serviceCenters: ServiceCenter[]) {
    serviceCenters.forEach(sc => {
      this.layers.addLayer(
        marker(sc.location, {
          icon: icon({
            iconSize: [45, 45],
            iconAnchor: [22.5, 45],
            iconUrl: sc.markerImage
          })
        }).bindPopup(
          `
                <img src="${sc.image}" alt="${sc.title} مرکز خدماتی" />
                <div class="left-section">
                <h3>${sc.title}</h3>
                <p>
                    ${sc.description}
                </p>
                <p>
                    <i class="icon icon-marker"></i>
                    ${sc.address}
                </p>
                <div class="link-box">
                    <a href="#/request/fix/send/${
                      sc.id
                    }" class="send-request-btn">رزرو</a>
                    <a href="#/service-center/${
                      sc.id
                    }" class="show-details-btn">مشاهده</a>
                </div>
                </div>
                    
            `,
          {
            offset: point(0, -30),
            className: "marker-popup",
            maxWidth: 400,
            minWidth: 400
          }
        )
      );
    });
  }

  onMapReady(map) {
    this.layers.addTo(map);
    map.on("moveend", () => {
      this.mapMoved(map.getCenter());
    });
  }

  mapMoved(center) {
    if (!this.allowChange) {
      this.allowChange = true;
      return;
    }
    console.log("mapMoved");
    this._serviceCenterListService.serviceCenterFilter.longitude = center.lng;
    this._serviceCenterListService.serviceCenterFilter.latitude = center.lat;
    this._serviceCenterListService.getServiceCenters().subscribe();
  }

  resetMarkers() {
    this.layers.clearLayers();
  }

  public get serviceCenters() {
    return this._serviceCenterListService.serviceCenterRepo;
  }
}
