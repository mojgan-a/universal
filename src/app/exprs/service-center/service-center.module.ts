import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { DpDatePickerModule } from 'ng2-jalali-date-picker';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { WidgetsModule } from '../../widgets/widgets.module';
import { AddEditComponent } from './add-edit/add-edit.component';
import { AddEditService } from './add-edit/add-edit.service';
import { AttachmentComponent } from './add-edit/attachment/attachment.component';
import { ConfirmPriceComponent } from './add-edit/confirm-price/confirm-price.component';
import { DetailesComponent } from './add-edit/detailes/detailes.component';
import { MembersComponent } from './add-edit/members/members.component';
import { ServicesComponent } from './add-edit/services/services.component';
import { SubmitBoxComponent } from './add-edit/submit-box/submit-box.component';
import { WorkingHoursComponent } from './add-edit/working-hours/working-hours.component';
import { MyServiceCentersComponent } from './my-service-centers/my-service-centers.component';
import { MyServiceCentersService } from './my-service-centers/my-service-centers.service';
import { CardViewComponent } from './service-center-list/card-view/card-view.component';
import { FilterBoxComponent } from './service-center-list/filter-box/filter-box.component';
import { MapViewComponent } from './service-center-list/map-view/map-view.component';
// import { ReserveComponent } from './service-center-list/reserve/reserve.component';
import { ServiceCenterListService } from './service-center-list/service-center-list.service';
import { ServiceCenterComponent } from './service-center.component';
import { routing } from './service-center.routing';
import { SingleComponent } from './single/single.component';
import { SingleService } from './single/single.service';

@NgModule({
    imports: [
        CommonModule,
        WidgetsModule,
        DpDatePickerModule,
        FormsModule,
        ReactiveFormsModule,
        InfiniteScrollModule,
        CurrencyMaskModule,
        LeafletModule,
        routing
    ],
    declarations: [
        ServiceCenterComponent,
        SingleComponent,
        MapViewComponent,
        CardViewComponent,
        FilterBoxComponent,
        AddEditComponent,
        MembersComponent,
        ServicesComponent,
        DetailesComponent,
        MyServiceCentersComponent,
        AttachmentComponent,
        SubmitBoxComponent,
        WorkingHoursComponent,
        ConfirmPriceComponent
        // ReserveComponent
    ],
    providers: [
        SingleService,
        ServiceCenterListService,
        MyServiceCentersService,
        AddEditService
    ]
})
export class ServiceCenterModule {}
