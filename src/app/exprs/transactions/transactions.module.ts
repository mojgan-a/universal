import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransactionsComponent } from './transactions.component';
import { routing } from './transactions.routing';
import { TransactionListComponent } from './transaction-list/transaction-list.component';

@NgModule({
    imports: [CommonModule, routing],
    declarations: [TransactionsComponent, TransactionListComponent]
})
export class TransactionsModule {}
