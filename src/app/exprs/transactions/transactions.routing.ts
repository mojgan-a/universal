import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TransactionListComponent } from './transaction-list/transaction-list.component';
import { TransactionsComponent } from './transactions.component';

export const routes: Routes = [
    {
        path: '',
        component: TransactionsComponent,
        children: [
            {
                path: 'transaction-list',
                component: TransactionListComponent
            },
            { path: '', redirectTo: 'transaction-list', pathMatch: 'full' }
        ]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
