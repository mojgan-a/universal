
import {of as observableOf,  Observable } from 'rxjs';
import * as _ from 'lodash';
import { environment } from '../../environments/environment';
import { ValidationResponse } from './general.model';

export class BaseService {
    protected _apiUrl = {
        reserve:    '../api/vms/repair',
        profile: '../api/user/profile',
        userImage: '../api/user/image',
        getImage: _.template('../api/file/avatar/<%= id %>'),
        defaultUserImage: '/assets/images/avatar.png',
        register: '../api/sign-up',
        confirm: '../api/confirm/mobile',
        checkUserName: '../api/login/check',
        login: '../api/login',
        getServiceCenters: '../api/vms/provider/search',
        getServiceGroup: '../api/vms/template/service/category',
        getServices: '../api/vms/template/service',
        passwordReset: '../api/password/reset',
        passwordForget: '../api/password/forget',
        vmsBrand: '../api/vms/brand',
        vmsColor: '../api/vms/color',
        vmsCar: '../api/vms/car',
        uploadServiceCenterImage: '../api/vms/provider/image',
        serviceCenterC: '../api/vms/provider',
        serviceCenterRUD: _.template('../api/vms/provider/<%= id %>'),
        serviceCenterServiceC: _.template(
            '../api/vms/provider/<%= provider %>/service'
        ),
        serviceCenterServiceUD: _.template(
            '../api/vms/provider/<%= provider %>/service/<%= service %>'
        ),
        requestCR: '../api/vms/request',
        requestUD: '../api/vms/request/<%= provider %>',
        uploadFile: '../api/file/upload',
        readFile: _.template('../api/file/download/<%= file %>'),
        smsSecretKey: '../api/secret-key',
        getProviderService: _.template(
            '../api/vms/provider/<%= provider %>/services'
        )
    };

    protected _baseUrl = environment.baseUrl;

    constructor() {}

    protected _catchBadResponse(err: any, source: Observable<any>) {
        if (!(err instanceof Error)) {
            return observableOf(
                new ValidationResponse(err.error.message, false)
            );
        } else {
            throw err;
        }
    }
}
