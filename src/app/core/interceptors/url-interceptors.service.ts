import {
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest
} from '@angular/common/http';
import { Injectable } from '@angular/core';


import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

const BaseUrl = environment.baseUrl;

@Injectable()
export class UrlInterceptor implements HttpInterceptor {
    private baseUrl = BaseUrl;
    intercept(
        req: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        if (req.url && req.url.slice(0, 2) === '..') {
            const fixedBaseReq = req.clone({
                url: this.baseUrl + req.url.substring(3)
            });
            return next.handle(fixedBaseReq);
        }
        return next.handle(req);
    }
}
