import {
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest
} from '@angular/common/http';
import { Injectable } from '@angular/core';


import { Observable } from 'rxjs';

@Injectable()
export class JsonInterceptor implements HttpInterceptor {
    intercept(
        req: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        if (!req.headers.get('Content-Type')) {
            const jsonReq = req.clone({
                headers: req.headers.set('Content-Type', 'application/json')
            });
            return next.handle(jsonReq);
        } else {
            const mfReq = req.clone({
                headers: req.headers.set('Content-Type', 'multipart/form-data')
            });
            return next.handle(mfReq);
        }
    }
}
