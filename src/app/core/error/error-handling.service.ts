import { Injectable } from '@angular/core';
import { errorMessages } from './errors';

@Injectable()
export class ErrorHandlingService {
    constructor() {}

    getErrorMessage(key) {
        return errorMessages[key];
    }
}
