export const errorMessages = {
  'required': 'این گزینه ضروری است.',
  'minlength': 'عبارت مورد استفاده کوتاه است.',
  'MatchPassword': 'تکرار رمز عبور اشتباه است',
  'fileTypeNotAllowed': 'فرمت فایل ارسالی اشتباه است.',
  'fileSizeLarge': 'سایز فایل انتخاب شده بیش از حد مجاز است.',
  'Not Found': 'مشکل در فراخوانی api!',
  'UserNotFound': 'کاربر شناسایی نشد.'
};
