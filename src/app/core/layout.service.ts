
import {map} from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';






import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { AuthService } from './auth.service';
import { BaseService } from './base-service';
import { SocketService } from './socket.service';

@Injectable()
export class LayoutService extends BaseService {
    constructor(
        private http: HttpClient,
        private socket: SocketService,
        private auth: AuthService,
        private router: Router
    ) {
        super();
    }

    getUserImagePath(): Observable<string> {
        return this.http.get('../api/user/profile').pipe(map((res: any) => {
            if (res.imageId) {
                return (
                    environment.baseUrl +
                    this._apiUrl.getImage({ id: res.imageId }).slice(3)
                );
            } else {
                return 'assets/images/avatar.png';
            }
        }));
    }

    logout() {
        this.auth.logout();
        this.router.navigate(['/']);
    }

    isLogin() {
        return this.auth.authStatus;
    }

    isLoginNow() {
        return this.auth.isLogedin();
    }

    getNotifications() {
        return this.socket.get('notification');
    }
}
