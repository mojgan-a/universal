import { EventEmitter, Injectable } from '@angular/core';
// import { User } from './general.model';

interface CurrentUser {
    id: string;
    phoneNumber: string;
    firstName: string;
    lastName: string;
    imageId: string;
}

@Injectable()
export class AuthService {
    private token: string;
    public user: CurrentUser;
    private _isLogedin = false;
    public authStatus = new EventEmitter();
    constructor() {
        const currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser) {
            this._isLogedin = true;
            this.authStatus.emit(this._isLogedin);
            this.token = currentUser.token;
            this.user = currentUser;
        } else {
            this._isLogedin = false;
            this.authStatus.emit(this._isLogedin);
        }
    }

    isLogedin() {
        return this._isLogedin;
    }

    login(token: string, user: CurrentUser) {
        this.token = token;
        const currentUser = Object.assign(
            {
                token: token
            },
            user
        );
        this.user = currentUser;
        localStorage.setItem('currentUser', JSON.stringify(currentUser));
        this._isLogedin = true;
        this.authStatus.emit(this._isLogedin);
    }

    logout() {
        localStorage.removeItem('currentUser');
        this.token = null;
        this.user = null;
        this._isLogedin = false;
        this.authStatus.emit(this._isLogedin);
    }

    getToken(): string {
        if (this.token) {
            return this.token;
        }

        return null;
    }

    getUser(): CurrentUser {
        return this.user;
    }
}
