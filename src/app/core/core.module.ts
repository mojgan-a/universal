import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AuthGuard } from '../core/auth.guard';
import { AuthService } from './auth.service';
import { ErrorHandlingService } from './error/error-handling.service';
import { AuthInterceptor, UrlInterceptor } from './interceptors';
import { LayoutService } from './layout.service';
import { NavigationService } from './navigation/navigation.service';
import { SocketService } from './socket.service';


@NgModule({
    imports: [CommonModule, HttpClientModule],
    declarations: [],
    providers: [
        AuthService,
        SocketService,
        AuthGuard,
        ErrorHandlingService,
        NavigationService,
        LayoutService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: UrlInterceptor,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true
        }
    ]
})
export class CoreModule {}
