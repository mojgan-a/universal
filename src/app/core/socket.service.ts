import { Injectable, OnDestroy } from '@angular/core';






import { Observable } from 'rxjs';
import * as io from 'socket.io-client';
import { AuthService } from './auth.service';


@Injectable()
export class SocketService implements OnDestroy {
    private url = 'http://localhost:8080/';
    private options: any = {
        query: {},
        reconnectionAttempts: 1
    };
    private socket;
    private isLive = false;

    constructor(private auth: AuthService) {
        // this.options.query.token = this.auth.getToken();
        // this.socket = io(this.url, this.options);
        // this.socket.on('reconnect_attempt', () => {
        //     this.socket.io.opts.query = {
        //         token: this.auth.getToken()
        //     };
        // });
        // this.socket.on('disconnect', reason => {
        //     this.isLive = false;
        // });
        // this.socket.on('connect', attemptNumber => {
        //     this.isLive = true;
        // });
    }

    get(event: string) {
        // if (!this.isLive) {
        //   throw(new Error('websocket is disconnected!'));
        // }

        const observable = new Observable(observer => {
            this.socket.on(event, data => {
                observer.next(data);
            });
            this.socket.on(event, data => {
                observer.next(data);
            });
            return () => {
                this.socket.disconnect();
            };
        });

        return observable;
    }

    send(event: string, data: any) {
        // if (!this.isLive) {
        //   throw(new Error('websocket is disconnected!'));
        // }

        const msg = JSON.stringify(data);
        this.socket.emit(event, msg);
    }

    ngOnDestroy() {
        this.socket.disconnect();
    }
}
