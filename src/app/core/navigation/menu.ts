export interface Link {
    title: string;
    link: string;
    icon?: string;
    children?: Nav;
}

export interface Nav {
    [index: number]: Link;
}

export interface Navigations {
    [title: string]: Nav;
}

export const navigations: Navigations = {
    'side-bar': [
        {
            title: 'مراکز خدماتی',
            icon: 'icon-map',
            link: '/service-center/map-view'
        },
        {
            title: ' درخواست سرویس ',
            icon: 'icon-request',
            link: '/request/sms'
        },
        {
            title: ' لیست درخواست ها',
            icon: 'icon-request-list',
            link: '/request/list'
        },
        // {
        //   title: ' لیست تراکنش ها',
        //   icon: 'icon-transaction-list',
        //   link: '/vehicle',
        // },
        {
            title: ' خودروهای من',
            icon: 'icon-cars',
            link: '/vehicle'
        },
        {
            title: ' تعمیرگاه من',
            icon: 'icon-my-car-repair-shop',
            link: '/service-center/my-service-centers'
        },
        {
            title: 'تنظیمات',
            icon: 'icon-setting',
            link: '/user/profile'
        }
        // {
        //   title: 'مشخصات کاربری',
        //   icon: '',
        //   link: '/user/profile',
        // },
        // {
        //   title: 'اطلاعات مالی',
        //   icon: 'walet-icon',
        //   link: '/transactions',
        // },
    ],
    header: [
        {
            title: 'asd',
            link: 'fb.com'
        }
    ]
};
