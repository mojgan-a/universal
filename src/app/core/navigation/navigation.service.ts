import { Injectable } from '@angular/core';
import { Link, Nav, navigations } from './menu';
export { Nav, Link };

@Injectable()
export class NavigationService {
    constructor() {}

    getNav(name: string): Nav {
        return navigations[name];
    }
}
