import * as _ from 'lodash';

export class ValidationResponse {
    constructor(
        public message: string,
        public ok?: boolean,
        public data?: any
    ) {
        if (_.isNil(this.ok)) {
            this.ok = true;
        }
    }
}

export class Address {
    constructor(
        public address: string,
        public country?: string,
        public city?: string,
        public location?: [number, number]
    ) {}
}
