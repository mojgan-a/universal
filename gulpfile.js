var gulp = require('gulp');
var sass = require('gulp-sass');
var connect = require('gulp-connect');
var sourcemaps = require('gulp-sourcemaps');
var pjson = require('./package.json');
var iconfont = require('gulp-iconfont');
var consolidate = require('gulp-consolidate');
var async = require('async');
var rename = require('gulp-rename');

function mapGlyphs(glyph) {
    return { name: glyph.name, codepoint: glyph.unicode[0].charCodeAt(0) };
}

gulp.task('fontIcon', function() {
    const fontName = pjson.name + '-icons';

    var iconStream = gulp.src(['src/assets/icons/*.svg']).pipe(
        iconfont({
            fontName: fontName,
            formats: ['ttf', 'eot', 'woff', 'woff2', 'svg'],
            normalize: true,
            fontHeight: 2000
        })
    );

    async.parallel(
        [
            function handleGlyphs(cb) {
                iconStream.on('glyphs', function(glyphs, options) {
                    gulp.src('templates/fontawesome-style.css')
                        .pipe(
                            consolidate('lodash', {
                                glyphs: glyphs.map(mapGlyphs),
                                fontName: fontName,
                                fontPath: 'fonts',
                                className: 'icon'
                            })
                        )
                        .pipe(rename({ basename: 'icons' }))
                        .pipe(gulp.dest('src/assets/fonts/fontIcon'))
                        .on('finish', cb);
                });
            },
            function handleSample(cb) {
                iconStream.on('glyphs', function(glyphs, options) {
                    gulp.src('templates/fontawesome-style.html')
                        .pipe(
                            consolidate('lodash', {
                                glyphs: glyphs.map(mapGlyphs),
                                fontName: fontName,
                                className: 'icon'
                            })
                        )
                        .pipe(rename({ basename: 'icons' }))
                        .pipe(gulp.dest('demos/'))
                        .on('finish', cb);
                });
            },
            function handleFonts(cb) {
                iconStream
                    .pipe(gulp.dest('src/assets/fonts/fontIcon/fonts'))
                    .on('finish', cb);
            }
        ],
        () => {
            console.log('fontIcon Created');
        }
    );
});

gulp.task('build', function() {
    return gulp
        .src('./src/assets/scss/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./demos/kit'))
        .pipe(connect.reload());
});

gulp.task('connect', function() {
    connect.server({
        root: './demos/kit',
        livereload: true
    });
});

gulp.task('watch', ['connect'], function() {
    gulp.watch('./src/**/*.scss', ['build']);
});

gulp.task('default', ['fontIcon', 'build']);
